<xsl:stylesheet version="1.0"
                xmlns:dictiography="com.google.code.elex.shared.ElexUtils"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
        >


    <xsl:template name="grammar.by">
        <xsl:variable name="pos">
            <xsl:value-of select="object/void[@property='posKey']/string/text()"/>
        </xsl:variable>
        <xsl:variable name="gender">
            <xsl:value-of select="object/void[@property='genderKey']/string/text()"/>
        </xsl:variable>
        <xsl:variable name="alleenMeer">
            <xsl:value-of select="object/void[@property='alleenMeer']/boolean/text()"/>
        </xsl:variable>
        <xsl:variable name="alleenEnkel">
            <xsl:value-of select="object/void[@property='alleenEnkel']/boolean/text()"/>
        </xsl:variable>

        <xsl:if test="$pos!=''">
            <span class="grammar_block">
                <xsl:value-of select="dictiography:getProperty($lang,$pos)"/>
                <xsl:if test="$gender='m'">,&#160;мужчынскі род</xsl:if>
                <xsl:if test="$gender='v'">,&#160;жаночы род</xsl:if>
                <xsl:if test="$gender='o'">,&#160;ніякі род</xsl:if>
            </span>
        </xsl:if>
        <xsl:if test="$pos=''">
            <span class="inline_grammar_block">
                <xsl:if test="$alleenEnkel='true'">адзіночны лік</xsl:if>
                <xsl:if test="$alleenMeer='true'">множны лік</xsl:if>
            </span>
        </xsl:if>
    </xsl:template>
</xsl:stylesheet>