package com.google.code.elex.api;

import java.util.Iterator;

/**
 * Author: Vitaly Sazanovich
 * Email: Vitaly.Sazanovich@gmail.com
 * Date: 2/13/14
 */
public interface IPeekIterator extends Iterator {
    public String peek();
}
