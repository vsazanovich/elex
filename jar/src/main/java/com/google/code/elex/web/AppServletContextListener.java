package com.google.code.elex.web;

/**
 * Author: Vitaly Sazanovich
 * Email: vitaly.sazanovich@gmail.com
 * Date: 24/03/14
 * Time: 11:49
 */

import com.google.code.elex.service.DictionariesPool;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

public class AppServletContextListener implements ServletContextListener {

    @Override
    public void contextDestroyed(ServletContextEvent arg0) {
        System.out.println("ServletContextListener destroyed");
    }

    @Override
    public void contextInitialized(ServletContextEvent arg0) {
        try {
            DictionariesPool.getInstance();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}