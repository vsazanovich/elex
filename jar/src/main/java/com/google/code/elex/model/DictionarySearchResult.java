package com.google.code.elex.model;

/**
 * Author: Vitaly Sazanovich
 * Email: vitaly.sazanovich@gmail.com
 * Date: 27/02/14
 * Time: 18:39
 */
public class DictionarySearchResult {
    int dictionaryId;
    float score;
    String headword;
    String text;
}
