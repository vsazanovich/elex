package com.google.code.elex.model;

import java.io.Serializable;

/**
 * Author: Vitaly Sazanovich
 * Email: vitaly.sazanovich@gmail.com
 * Date: 28/11/13
 */
public class DictionarySectionDescriptor implements Comparable<DictionarySectionDescriptor>,Serializable {
    public int size;
    public int offset;
    public String name;

    @Override
    public int compareTo(DictionarySectionDescriptor o) {
        if (offset > o.offset) {
            return -1;
        } else if (offset < o.offset) {
            return 1;
        } else {
            return 0;
        }
    }
}
