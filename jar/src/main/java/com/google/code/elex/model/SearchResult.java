package com.google.code.elex.model;

import java.util.ArrayList;
import java.util.List;
import java.util.SortedMap;
import java.util.TreeMap;

/**
 * Author: Vitaly Sazanovich
 * Email: vitaly.sazanovich@gmail.com
 * Date: 27/02/14
 * Time: 18:17
 */
public class SearchResult {
    public SortedMap<Float, List<DictionarySearchResult>> results = new TreeMap<Float, List<DictionarySearchResult>>();

    public List<DictionarySearchResult> getResults() {
        List<DictionarySearchResult> res = new ArrayList<DictionarySearchResult>();
        for (Float key : results.keySet()) {
            res.addAll(results.get(key));
        }
        return res;
    }
}
