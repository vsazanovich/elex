package com.google.code.elex.service;

import com.google.code.elex.api.Constants;
import com.google.code.elex.model.*;
import org.apache.commons.io.monitor.FileAlterationListenerAdaptor;
import org.apache.commons.io.monitor.FileAlterationMonitor;
import org.apache.commons.io.monitor.FileAlterationObserver;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopScoreDocCollector;
import org.apache.lucene.util.Version;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.text.Collator;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Author: Vitaly Sazanovich
 * Email: vitaly.sazanovich@gmail.com
 * Date: 27/11/13
 */
public class DictionariesPool extends FileAlterationListenerAdaptor {
    private static final Logger logger = Logger.getLogger(DictionariesPool.class.getName());
    public static Map<Integer, CompressedDictionary> DICTIONARIES = Collections.synchronizedMap(new HashMap<Integer, CompressedDictionary>());
    private FileAlterationObserver observer;
    private static DictionariesPool INSTANCE;


    public static DictionariesPool getInstance() throws Exception {
        if (INSTANCE == null) {
            INSTANCE = new DictionariesPool();
        }
        return INSTANCE;
    }

    private DictionariesPool() throws Exception {
        String contentFolderName = Constants.ELEX_PROPS.getProperty("content.folder");
        String cacheFolderName = Constants.ELEX_PROPS.getProperty("cache.folder");

        File zipFolder = new File(contentFolderName);
        File cacheFolder = new File(cacheFolderName);

        if (!zipFolder.exists()) {
            zipFolder.mkdirs();
        }
        if (!zipFolder.exists()) {
            throw new RuntimeException("Could not create content folder. Do you have privileges to do that?");
        }

        if (!cacheFolder.exists()) {
            cacheFolder.mkdirs();
        }
        if (!cacheFolder.exists()) {
            throw new RuntimeException("Could not create cache folder. Do you have privileges to do that?");
        }
        update();
        observer = new FileAlterationObserver(zipFolder);
        observer.addListener(this);

        long interval = 1000;
        FileAlterationMonitor monitor = new FileAlterationMonitor(interval);
        monitor.addObserver(observer);
        monitor.start();
    }

    private void update() {
        try {
            for (CompressedDictionary cd : DICTIONARIES.values()) {
                cd.inputStream.close();
            }
            DICTIONARIES.clear();
            String contentFolderName = Constants.ELEX_PROPS.getProperty("content.folder");
            File zipFolder = new File(contentFolderName);
            explore(zipFolder);
            removeOrphans();
            loadResources(zipFolder);
            System.gc();
        } catch (IOException e) {
            logger.log(Level.SEVERE, "Couldn't update state", e);
        }
    }

    /**
     * Removing cache files that do not have corresponding zip files
     *
     * @throws IOException
     */
    private void removeOrphans() throws IOException {
        String contentFolderS = Constants.ELEX_PROPS.getProperty("content.folder");
        String cacheFoldeS = Constants.ELEX_PROPS.getProperty("cache.folder");

        File cacheFolder = new File(cacheFoldeS);
        File contentFolder = new File(contentFolderS);

        File[] cacheFiles = cacheFolder.listFiles();
        for (File f : cacheFiles) {
            CompressedDictionary cd = getDictionaryByCacheName(f.getName());
            if (cd != null) {
                File dicFile = new File(contentFolder + File.separator + cd.getProperties().getProperty(Constants.DICTIONARY_FILE_NAME));
                if (!dicFile.exists()) {
                    cd.inputStream.close();
                    f.delete();
                    Integer dicId = (Integer) cd.getProperties().get(Constants.DICTIONARY_ID);
                    DICTIONARIES.remove(dicId);
                }
            } else {
                f.delete();
            }

        }
    }

    private CompressedDictionary getDictionaryByCacheName(String name) {
        for (CompressedDictionary cd : DICTIONARIES.values()) {
            if (cd.getCacheFileName().equals(name)) {
                return cd;
            }
        }
        return null;
    }

    public void explore(File zipFolder) {
        File[] dicFiles = zipFolder.listFiles();
        for (File dicFile : dicFiles) {
            if (dicFile.isDirectory()) {
                explore(dicFile);
            } else if (dicFile.isFile() && dicFile.getName().endsWith(".zip")) {
                try {
                    install(dicFile);
                } catch (Exception ex) {
                    logger.log(Level.WARNING, "Couldn't install the dictionary: " + dicFile.getAbsolutePath());
                }
            }
        }
    }

    private CompressedDictionary install(File file) throws Exception {
        String cacheFolderName = Constants.ELEX_PROPS.getProperty("cache.folder");
        CompressedDictionary cd = new CompressedDictionary(cacheFolderName, file.getPath());
        int nextAvailId = getNextAvailId();
        cd.getProperties().put(Constants.DICTIONARY_ID, nextAvailId);
        cd.getProperties().put(Constants.DICTIONARY_FILE_NAME, file.getName());
        DICTIONARIES.put(nextAvailId, cd);
        return cd;
    }

    public void loadResources(File f) {
        File[] ffs = f.listFiles();
        for (File file : ffs) {
            if (file.isDirectory()) {
            } else if (file.isFile() && file.getName().endsWith(".res")) {
                try {
                    CompressedDictionary cd = findDictionaryForResource(file.getName());
                    if (cd != null) {
                        cd.addResource(new ResourcesPackage(file.getAbsolutePath()));
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }
    }

    private CompressedDictionary findDictionaryForResource(String name) {
        for (CompressedDictionary cd : DICTIONARIES.values()) {
            Set<String> en = cd.getProperties().stringPropertyNames();
            for (String key : en) {
                if (key.startsWith("res.name") && cd.getProperties().getProperty(key).equals(name)) {
                    return cd;
                }
            }
        }
        return null;
    }

    public String[] getAvailableLanguages() {
        Set<String> langs = new HashSet<String>();
        for (CompressedDictionary cd : DICTIONARIES.values()) {
            String sl = cd.getProperties().getProperty(Constants.DICTIONARY_SOURCE_LANGUAGE);
            String tl = cd.getProperties().getProperty(Constants.DICTIONARY_TARGET_LANGUAGE);
            String langPair = sl.toLowerCase() + " ⇨ " + tl;
            langs.add(langPair);
        }
        return langs.toArray(new String[langs.size()]);
    }

    public Properties[] getDictionaries(String sourceLang, String targetLang) {
        Set<Properties> dics = new HashSet<Properties>();
        for (CompressedDictionary cd : DICTIONARIES.values()) {
            String sl = cd.getProperties().getProperty(Constants.DICTIONARY_SOURCE_LANGUAGE);
            String tl = cd.getProperties().getProperty(Constants.DICTIONARY_TARGET_LANGUAGE);
            if (sl.toLowerCase().equals(sourceLang.toLowerCase()) && tl.toLowerCase().equals(targetLang.toLowerCase())) {
                dics.add(cd.getProperties());
            }
        }
        return dics.toArray(new Properties[dics.size()]);
    }

    public CompressedDictionary getDictionaryById(Integer id) {
        for (CompressedDictionary cd : DICTIONARIES.values()) {
            Integer cdId = (Integer) cd.getProperties().get(Constants.DICTIONARY_ID);
            if (id.intValue() == cdId.intValue()) {
                return cd;
            }
        }
        return null;
    }

    public IndexView getIndexView(Set<CompressedDictionary> set, int pageSize, int viewOffset, int percent, Collator collator) throws Exception {
        String from = getHeadwordByPercent(set, percent, collator);
        return getIndexView(set, pageSize, viewOffset, from, collator);
    }

    public String scrollTo(int to, Set<CompressedDictionary> set, String searchValue, Collator collator) throws Exception {
        int counter = Math.abs(to);
        String retValue = searchValue;
        IteratorsWrapper iteratorsWrapper = new IteratorsWrapper(set, searchValue, to > 0 ? true : false, collator);

        while (counter >= 0 && iteratorsWrapper.hasNext()) {
            String val = iteratorsWrapper.next();
            if (val != null) {
                retValue = val;
                --counter;
            } else {
                break;
            }
        }
        return retValue == null ? searchValue : retValue;

    }

    public IndexView getIndexView(Set<CompressedDictionary> set, int pageSize, int viewOffset, String from, Collator collator) throws Exception {
        IndexView indexView = new IndexView();
        SortedSet<String> index = new TreeSet<String>(collator);

        IteratorsWrapper forwardIteratorsWrapper = new IteratorsWrapper(set, from, true, collator);
        IteratorsWrapper backwardIteratorsWrapper = new IteratorsWrapper(set, from, false, collator);


        int forwardCounter = pageSize - viewOffset;
        while (index.size() < pageSize && forwardIteratorsWrapper.hasNext() && forwardCounter > 0) {
            index.add(forwardIteratorsWrapper.next());
            --forwardCounter;
        }
        while (index.size() < pageSize && backwardIteratorsWrapper.hasNext()) {
            index.add(backwardIteratorsWrapper.next());
        }
        while (index.size() < pageSize && forwardIteratorsWrapper.hasNext()) {
            index.add(forwardIteratorsWrapper.next());
        }

        indexView.index = index;
        String minHw = getMinHeadword(set);
        String maxHw = getMaxHeadword(set);
        indexView.isStartReached = minHw == null || index.isEmpty() || minHw.equals(index.first());
        indexView.isEndReached = maxHw == null || index.isEmpty() || maxHw.equals(index.last());

        return indexView;
    }

    private String getMinHeadword(Set<CompressedDictionary> set) throws Exception {
        String minHw = null;
        for (CompressedDictionary cd : set) {
            String hw = cd.getHeadwordAtOffset(0);
            if (minHw == null || minHw.compareTo(hw) > 0) {
                minHw = hw;
            }
        }
        return minHw;
    }

    private String getMaxHeadword(Set<CompressedDictionary> set) throws Exception {
        String maxHw = null;
        for (CompressedDictionary cd : set) {
            String hw = cd.getHeadwordAtOffset(cd.getSize() - 1);
            if (maxHw == null || maxHw.compareTo(hw) < 0) {
                maxHw = hw;
            }
        }
        return maxHw;
    }

    public Map<Integer, String> getEntries(Set<Integer> set, String hw) throws Exception {
        Map<Integer, String> m = new HashMap<Integer, String>();
        for (Integer it : set) {
            String entry = DICTIONARIES.get(it).retrieveEntry(hw);
            if (entry != null) {
                m.put(it, entry);
            }
        }
        return m;
    }

    public void getDictionariesResourcesByType(OutputStream os, Set<CompressedDictionary> set, String type) throws Exception {
        for (CompressedDictionary cd : set) {
            DictionarySectionDescriptor section = cd.find(type);
            if (section == null) {
                continue;
            } else {
                cd.writeSectionBytes(os, section);
            }
        }
    }


    public int getPercentByHeadword(Set<CompressedDictionary> set, String hw) throws Exception {
        CompressedDictionary dicMax = getDicWithMaxSize(set);
        int hwOffset = Math.abs(dicMax.findInsertionPoint(hw));
        return (int) Math.round((double) hwOffset * 100 / (double) dicMax.getSize());
    }


    private String getHeadwordByPercent(Set<CompressedDictionary> set, int percent, Collator collator) throws Exception {
        if (percent == 0) {
            return getFirstHeadword(set, collator);
        } else if (percent == 100) {
            return getLastHeadword(set, collator);
        }
        CompressedDictionary dicMax = getDicWithMaxSize(set);
        return dicMax.getHeadwordAtPercent(percent);
    }

    private CompressedDictionary getDicWithMaxSize(Set<CompressedDictionary> set) {
        CompressedDictionary maxSizeCd = set.iterator().next();
        int max = maxSizeCd.getSize();
        for (CompressedDictionary cd : set) {
            int size = cd.getSize();
            if (size > max) {
                max = size;
                maxSizeCd = cd;
            }
        }
        return maxSizeCd;
    }

    private String getLastHeadword(Set<CompressedDictionary> set, Collator collator) throws Exception {
        SortedSet<String> hws = new TreeSet<String>(collator);
        for (CompressedDictionary cd : set) {
            String hw = cd.getHeadwordAtOffset(cd.getSize() - 1);
            hws.add(hw);
        }
        return hws.last();
    }

    private String getFirstHeadword(Set<CompressedDictionary> set, Collator collator) throws Exception {
        SortedSet<String> hws = new TreeSet<String>(collator);
        for (CompressedDictionary cd : set) {
            String hw = cd.getHeadwordAtOffset(0);
            hws.add(hw);
        }
        return hws.first();
    }


    public String findHeadwordByIndexedFieldValue(String shortDictionaryName, String searchField, String searchValue) throws Exception {
        CompressedDictionary cd = getDictionaryByShortName(shortDictionaryName);
        Analyzer analyzer = ElexUtils.getAnalyzerByLanguage(cd.getProperties().getProperty(Constants.DICTIONARY_TARGET_LANGUAGE));
        Query q = new QueryParser(Version.LUCENE_46, searchField, analyzer).parse(searchValue);

        int hitsPerPage = 10;

        IndexReader reader = DirectoryReader.open(cd.getDirectory());
        IndexSearcher searcher = new IndexSearcher(reader);
        TopScoreDocCollector collector = TopScoreDocCollector.create(hitsPerPage, true);
        searcher.search(q, collector);
        ScoreDoc[] hits = collector.topDocs().scoreDocs;
        if (hits.length > 0) {
            if (hits.length == 1) {
                return searcher.doc(hits[0].doc).get("headword");
            } else {//finding the best match
                SortedMap<Integer, String> m = new TreeMap<Integer, String>();
                String bestMatch = searcher.doc(hits[0].doc).get("headword");
                m.put(1, bestMatch);
                for (int i = 0; i < hits.length; ++i) {
                    int docId = hits[i].doc;
                    Document d = searcher.doc(docId);
                    String hw = d.get("headword");
                    if (hw.equals(searchValue)) {
                        m.put(10, hw);
                    } else if (hw.toLowerCase().equals(searchValue)) {
                        m.put(9, hw);
                    } else if (hw.toLowerCase().equals(searchValue.toLowerCase())) {
                        m.put(8, hw);
                    } else if (hw.startsWith(searchValue)) {
                        m.put(7, hw);
                    } else if (hw.toLowerCase().startsWith(searchValue)) {
                        m.put(6, hw);
                    } else if (hw.toLowerCase().startsWith(searchValue.toLowerCase())) {
                        m.put(5, hw);
                    } else if (hw.toLowerCase().indexOf(searchValue.toLowerCase()) > 0) {
                        m.put(4, hw);
                    }
                }
                return m.get(m.lastKey());
            }
        } else {
            logger.log(Level.WARNING, "Couldn't find headwords for query: " + searchValue);
            return null;
        }
    }

    public CompressedDictionary getDictionaryByShortName(String shortDictionaryName) {
        for (CompressedDictionary cd : DICTIONARIES.values()) {
            String sdn = cd.getProperties().getProperty(Constants.DICTIONARY_NAME_SHORT);
            if (sdn.equals(shortDictionaryName)) {
                return cd;
            }
        }
        return null;
    }

    public SearchResult search(Set<CompressedDictionary> set, String searchValue) throws Exception {
        SearchResult sr = new SearchResult();
        for (CompressedDictionary cd : set) {
            Map<? extends Float, ? extends List<DictionarySearchResult>> m = cd.search(searchValue);
            for (Float key : m.keySet()) {
                List<DictionarySearchResult> newRes = m.get(key);
                List<DictionarySearchResult> origRes = sr.results.get(key);
                if (origRes == null) {
                    origRes = new ArrayList<DictionarySearchResult>();
                }
                origRes.addAll(newRes);
                sr.results.put(key, origRes);
            }
        }
        return sr;
    }

    public int getNextAvailId() {
        int nextAvailId = 0;
        for (CompressedDictionary cd : DICTIONARIES.values()) {
            int dicId = ((Integer) cd.getProperties().get(Constants.DICTIONARY_ID)).intValue();
            if (dicId > nextAvailId) {
                nextAvailId = dicId;
            }
        }
        return nextAvailId + 1;
    }

    @Override
    public void onFileCreate(final File file) {
        logger.log(Level.INFO, file.getAbsolutePath());
        update();
    }

    @Override
    public void onFileChange(final File file) {
        update();
    }

    @Override
    public void onFileDelete(final File file) {
        update();
    }
}
