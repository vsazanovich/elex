package com.google.code.elex.web;

import com.google.code.elex.api.Constants;
import com.google.code.elex.model.CompressedDictionary;
import com.google.code.elex.service.DictionariesPool;
import com.google.code.elex.service.ElexUtils;
import com.google.gson.Gson;
import org.apache.commons.lang3.CharEncoding;
import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.highlight.Highlighter;
import org.apache.lucene.search.highlight.NullFragmenter;
import org.apache.lucene.search.highlight.QueryScorer;
import org.apache.lucene.search.highlight.SimpleHTMLFormatter;
import org.apache.lucene.util.Version;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * Author: Vitaly Sazanovich
 * Email: Vitaly.Sazanovich@gmail.com
 * Date: 11/24/13
 */
public class EntryServlet extends ElexServlet {

    protected void doGet(HttpServletRequest request,
                         HttpServletResponse response) throws ServletException, IOException {
        OutputStreamWriter osw = new OutputStreamWriter(response.getOutputStream(), CharEncoding.UTF_8);
        PrintWriter out = new PrintWriter(osw, true);
        try {
            request.setCharacterEncoding(CharEncoding.UTF_8);
            response.setCharacterEncoding(CharEncoding.UTF_8);
            response.setContentType("application/json; charset=UTF-8");

            String[] dicIdsStr = request.getParameter("dics").split("_");
            Set<Integer> set = new HashSet<Integer>();
            for (String di : dicIdsStr) {
                if (!di.equals("")) {
                    set.add(Integer.parseInt(di));
                }
            }

            String searchValue = StringEscapeUtils.unescapeXml(request.getParameter("sv"));
            boolean hightlightOn = Boolean.parseBoolean(request.getParameter("hl"));
            String hw = StringEscapeUtils.unescapeXml(request.getParameter("hw"));

            Gson gson = new Gson();
            Map<Integer, String> entries = DictionariesPool.getInstance().getEntries(set, hw);
            if (hightlightOn && searchValue != null && !searchValue.equals("")) {
                for (Integer key : entries.keySet()) {
                    CompressedDictionary cd = DictionariesPool.getInstance().getDictionaryById(key);
                    Analyzer analyzer = ElexUtils.getAnalyzerByLanguage(cd.getProperties().getProperty(Constants.DICTIONARY_TARGET_LANGUAGE));
                    Query q = new QueryParser(Version.LUCENE_46, "content", analyzer).parse(searchValue);
                    SimpleHTMLFormatter formatter = new SimpleHTMLFormatter("<span class=\"highlight\">", "</span>");
                    Highlighter highlighter = new Highlighter(formatter, new QueryScorer(q));
                    highlighter.setTextFragmenter(new NullFragmenter());
                    String entry = entries.get(key);
                    //wrapping text elements in highlight tag
                    Document doc = ElexUtils.loadXMLFromString(entry);
                    fixNode(doc, doc.getDocumentElement(), highlighter, analyzer);
                    entry = ElexUtils.convertToXml(doc);
                    entry = entry.replaceAll("&lt;span class=\"highlight\"&gt;","<span class=\"highlight\">");
                    entry = entry.replaceAll("&lt;/span&gt;","</span>");
                    entries.put(key, entry);
                }
            }
            String s = gson.toJson(entries);
            out.write(s);

        } catch (Exception e) {
            e.printStackTrace();
            out.write(e.getMessage());
        } finally {
            osw.flush();
            out.flush();
            osw.close();
            out.close();
        }
    }

    private void fixNode(Document doc, Node n, Highlighter highlighter, Analyzer analyzer) throws Exception {
        if (n.getNodeType() == Node.TEXT_NODE) {
            String textNode = n.getNodeValue();
            textNode = highlighter.getBestFragment(analyzer, "", textNode);
            if (textNode != null) {
                n.setNodeValue(textNode);
            }
        }
        NodeList nl = n.getChildNodes();
        for (int i = 0; i < nl.getLength(); i++) {
            fixNode(doc, nl.item(i), highlighter, analyzer);
        }
    }

}
