package com.google.code.elex.web;

import com.google.code.elex.api.Constants;
import com.google.code.elex.service.DictionariesPool;
import com.google.gson.Gson;
import org.apache.commons.lang3.CharEncoding;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.util.Properties;

/**
 * Author: Vitaly Sazanovich
 * Email: vitaly.sazanovich@gmail.com
 * Date: 18/11/13
 */
public class InfoServlet extends ElexServlet {

    protected void doGet(HttpServletRequest request,
                         HttpServletResponse response) throws ServletException, IOException {
        OutputStreamWriter osw = new OutputStreamWriter(response.getOutputStream(), CharEncoding.UTF_8);
        PrintWriter out = new PrintWriter(osw, true);
        try {
            request.setCharacterEncoding(CharEncoding.UTF_8);
            response.setCharacterEncoding(CharEncoding.UTF_8);
            response.setContentType("application/json; charset=UTF-8");

            String action = request.getParameter("a");
            if (action == null) {
                throw new Exception(Constants.EXCEPTION_EMPTY);
            }

            if (action.equals("langs")) {
                Gson gson = new Gson();
                String[] langs = DictionariesPool.getInstance().getAvailableLanguages();
                String s = gson.toJson(langs);
                out.write(s);
            } else if (action.equals("dics")) {
                String sourceLang = request.getParameter("sl").toLowerCase();
                String targetLang = request.getParameter("tl").toLowerCase();
                if (sourceLang == null || targetLang == null) {
                    throw new Exception(Constants.EXCEPTION_EMPTY);
                } else {
                    Gson gson = new Gson();
                    Properties[] dics = DictionariesPool.getInstance().getDictionaries(sourceLang, targetLang);
                    String s = gson.toJson(dics);
                    out.write(s);
                }
            } else {
                throw new Exception(Constants.EXCEPTION_NOT_EXISTS);
            }

        } catch (Exception e) {
            e.printStackTrace();
            out.write(e.getMessage());
        } finally {
            osw.flush();
            out.flush();
            osw.close();
            out.close();
        }
    }

}

