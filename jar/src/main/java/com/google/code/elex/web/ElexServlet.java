package com.google.code.elex.web;

import com.google.code.elex.model.CompressedDictionary;
import com.google.code.elex.service.DictionariesPool;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import java.util.HashSet;
import java.util.Set;

/**
 * Author: Vitaly Sazanovich
 * Email: vitaly.sazanovich@gmail.com
 * Date: 09/12/13
 */
public abstract class ElexServlet extends HttpServlet {


    protected Set<CompressedDictionary> getDics(HttpServletRequest request) {
        if (request.getParameter("dics") == null) {
            Set<CompressedDictionary> set = new HashSet<CompressedDictionary>();
            set.addAll(DictionariesPool.DICTIONARIES.values());
            return set;
        }
        Set<CompressedDictionary> set = new HashSet<CompressedDictionary>();
        String[] dicIdsStr = request.getParameter("dics").split("_");
        for (String di : dicIdsStr) {
            if (!di.equals("")) {
                int id = Integer.parseInt(di);
                CompressedDictionary cd = DictionariesPool.DICTIONARIES.get(id);
                set.add(cd);
            }
        }
        return set;
    }
}
