package com.google.code.elex.pack;

import com.google.code.elex.service.ElexUtils;
import org.apache.commons.io.IOUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.security.DigestInputStream;
import java.security.MessageDigest;
import java.util.HashMap;
import java.util.Map;

/**
 * Author: Vitaly Sazanovich
 * Email: vitaly.sazanovich@gmail.com
 * Date: 11/02/14
 */
public class FileComparator {
    public static void main(String[] args) throws Exception {
        if (args.length != 2) {
            throw new Exception("2 args expected");
        }
        File f1 = new File(args[0]);
        File f2 = new File(args[1]);
        Map<String, String> m1 = new HashMap<String, String>();
        Map<String, String> m2 = new HashMap<String, String>();
        for (File f : f1.listFiles()) {
            m1.put(f.getName(), ElexUtils.getMD5Checksum(new FileInputStream(f)));
        }
        for (File f : f2.listFiles()) {
            m2.put(f.getName(), ElexUtils.getMD5Checksum(new FileInputStream(f)));
        }

        for (String k : m1.keySet()) {
            if (m2.get(k).equals(m1.get(k))) {

            } else {
                throw new Exception(k);
            }
        }
    }




}
