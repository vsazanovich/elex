package com.google.code.elex.shared;


import com.google.code.elex.client.ByteArrayPersistenceDelegate;
import com.google.code.elex.client.Utf8ResourceBundle;
import com.google.code.elex.client.entry.*;
import com.google.code.elex.client.model.MediaFile;
import com.sun.org.apache.xalan.internal.xsltc.DOM;

import javax.imageio.ImageIO;
import javax.imageio.stream.MemoryCacheImageInputStream;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import java.awt.image.BufferedImage;
import java.beans.PersistenceDelegate;
import java.beans.XMLDecoder;
import java.beans.XMLEncoder;
import java.io.*;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ElexUtils {
    private static final Logger logger = Logger.getLogger(ElexUtils.class.getName());

    private static String LEGAL_CHARACTERS = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
    private static char ESCAPE_SYMBOL = '_';
    private static int MAX_TREE_DEPTH = 3;
    private static int FOLDER_NAME_LENGTH = 2;
    private static String dataFolderName = "data";
    private static Properties PROPS;
    private static Properties WINSTONE_PROPS;

    public static Properties getProperties() {
        if (PROPS == null) {
            PROPS = new Properties();
            try {
                PROPS.load(ElexUtils.class.getClassLoader().getResourceAsStream("dictiography.properties"));
                File f = new File("dictiography.properties");
                if (f.exists()) {
                    Properties userProps = new Properties();
                    userProps.load(new FileInputStream(f));
                    for (Object key : userProps.keySet()) {
                        PROPS.put(key.toString(), userProps.get(key));
                    }
                }
            } catch (IOException e) {
                logger.log(Level.SEVERE, e.getMessage(), e);
            }
        }
        return PROPS;
    }

    public static Properties getWinstoneProperties() {
        if (WINSTONE_PROPS == null) {
            WINSTONE_PROPS = new Properties();
            try {
                WINSTONE_PROPS.load(ElexUtils.class.getClassLoader().getResourceAsStream("embedded.properties"));
                File f = new File("embedded.properties");
                if (f.exists()) {
                    Properties userProps = new Properties();
                    userProps.load(new FileInputStream(f));
                    for (Object key : userProps.keySet()) {
                        WINSTONE_PROPS.put(key.toString(), userProps.get(key));
                    }
                }
            } catch (IOException e) {
                logger.log(Level.SEVERE, e.getMessage(), e);
            }
        }
        return WINSTONE_PROPS;
    }

    public static boolean isValidHeadword(String headword) {
        if (headword == null ||
                headword.length() == 0 ||
                headword.length() > 100) {
            return false;
        }
        return true;
    }

    /**
     * @param headword
     * @return path to content file in the format: xx/xx/ww/ww/xxxxwwww.xml
     */
    public static String headwordToFilePath(String headword) {
        StringBuilder sb = new StringBuilder();
        int i = 0;
        int foldersCount = 0;
        StringBuilder tmp = new StringBuilder(2);
        if (headword.length() > FOLDER_NAME_LENGTH) {
            while (i < headword.length()) {
                char c = headword.charAt(i);
                if (LEGAL_CHARACTERS.indexOf(c) != -1) {
                    tmp.append(c);
                } else {
                    tmp.append(ESCAPE_SYMBOL);
                    tmp.append(Integer.toHexString(c));
                    tmp.append(ESCAPE_SYMBOL);
                }
                if ((i + 1) % FOLDER_NAME_LENGTH == 0) {
                    sb.append(tmp.toString());
                    tmp = new StringBuilder(2);
                    sb.append(File.separator);
                    ++foldersCount;
                    if (foldersCount >= MAX_TREE_DEPTH) {
                        break;
                    }
                }
                ++i;
            }
        }
        return sb.toString();
    }

    public static String headwordToFileName(String headword) {
        StringBuilder sb = new StringBuilder();
        for (char c : headword.toCharArray()) {
            if (LEGAL_CHARACTERS.indexOf(c) != -1) {
                sb.append(c);
            } else {
                sb.append(ESCAPE_SYMBOL);
                sb.append(Integer.toHexString(c));
                sb.append(ESCAPE_SYMBOL);
            }
        }
        return sb.toString();
    }

    public static String fileNameToHeadword(String fileName) throws Exception {
        StringBuilder sb = new StringBuilder();
        int i = 0;
        while (i < fileName.length()) {
            char c = fileName.charAt(i);
            if (LEGAL_CHARACTERS.indexOf(c) != -1) {
                sb.append(c);
            } else if (c == ESCAPE_SYMBOL) {
                StringBuilder num = new StringBuilder();
                ++i;
                while (fileName.charAt(i) != ESCAPE_SYMBOL) {
                    num.append(fileName.charAt(i));
                    ++i;
                }
                int intValue = Integer.parseInt(num.toString(), 16);
                sb.append((char) intValue);
            } else {
                throw new Exception("Illegal character found: " + c);
            }
            ++i;
        }
        return sb.toString();
    }


    public static String getSupportedLangs() {
        StringBuilder sb = new StringBuilder();
        for (String lang : getProperties().get("langs").toString().split(",")) {
            sb.append("<option value=\"" + lang.toUpperCase() + "\">" + lang.toUpperCase() + "</option>");
        }
        return sb.toString();
    }

    public static String getProperty(String lang, String key) {
        try {
            return Utf8ResourceBundle.getBundle("DictiographyResource", new Locale(lang.toLowerCase()), ElexUtils.class.getClassLoader()).getString(key);
        } catch (Exception e) {
            try {
                return Utf8ResourceBundle.getBundle("DictiographyResource", new Locale(ElexConstants.DEFAULT_LOCALE), ElexUtils.class.getClassLoader()).getString(key);
            } catch (Exception ex) {
                logger.log(Level.SEVERE, "Can't find resource for key: " + key);
                return key;
            }
        }
    }

    public static Properties getResourceBundle(String lang) {
        try {
            return wrapResourceBundle(Utf8ResourceBundle.getBundle("DictiographyResource", new Locale(lang), ElexUtils.class.getClassLoader()));
        } catch (Exception e) {
            return wrapResourceBundle(Utf8ResourceBundle.getBundle("DictiographyResource", new Locale(ElexConstants.DEFAULT_LOCALE), ElexUtils.class.getClassLoader()));
        }
    }

    private static Properties wrapResourceBundle(ResourceBundle rb) {
        Properties props = new Properties();
        for (String key : rb.keySet()) {
            props.put(key, rb.getString(key));
        }
        return props;
    }


    public static String stressSyllable(String wrapperTag, String className, String vowels, Object s, Object ss) {
        if (s == null || ((DOM) s).getStringValue().equals("")) return null;
        String syllables = ((DOM) s).getStringValue();
        int stressedSyllable = ((DOM) ss).getStringValue().equals("") ? 1 : Integer.parseInt(((DOM) ss).getStringValue());

        String[] vowelsArr = vowels.split(",");
        String[] syllablesArr = syllables.split("·|▪|\\||(?<=[0-9])|(?<=[-])");
        String[] patchedSyllablesArr = syllablesArr.clone();

        if (stressedSyllable <= syllablesArr.length && syllablesArr.length > 1) {
            patchedSyllablesArr[stressedSyllable - 1] = patch(wrapperTag, className, syllablesArr[stressedSyllable - 1], vowelsArr);//syllables start with 1
        }
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < syllablesArr.length; i++) {
            sb.append(patchedSyllablesArr[i]);
            if (i < syllablesArr.length - 1 && !isNumber(syllablesArr[i]) && !syllablesArr[i].endsWith("-")) {
                sb.append("·");
            }
        }
        return sb.toString();
    }

    private static boolean isNumber(String s) {
        if (s.isEmpty()) return false;
        if (s.length() > 1) return false;
        return Character.isDigit(s.charAt(0));
    }

    private static String patch(String wrapperTag, String className, String s, String[] vowels) {
        String stressMeVowel = null;
        for (String v : vowels) {
            if (s.contains(v)) {
                if (stressMeVowel==null){
                    stressMeVowel = v;
                }else{
                    int stressMeVowelIndex = s.indexOf(stressMeVowel);
                    if (s.indexOf(v)<stressMeVowelIndex){
                        stressMeVowel = v;
                    }
                }
            }
        }
        if (stressMeVowel!=null){
            s = s.replaceFirst(stressMeVowel, "<" + wrapperTag + " class=\"" + className + "\">" + stressMeVowel + "</" + wrapperTag + ">");
        }
        return s;
    }

    public static String links2str(EntryLink[] arr) {
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < arr.length; i++) {
            sb.append(arr[i].getText());
            if (i < arr.length - 1) {
                sb.append(",");
            }
        }
        return sb.toString();
    }

    public static EntryLink[] str2links(String str) {
        String[] ss = str.split(",");
        SortedSet<EntryLink> els = new TreeSet<EntryLink>();
        for (String s : ss) {
            EntryLink el = new EntryLink();
            el.setText(s.trim());
            els.add(el);
        }
        return els.toArray(new EntryLink[els.size()]);
    }

    public static byte[] bufferedImageToBytes(String fileExt, BufferedImage bi) {
        try {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            ImageIO.write(bi, fileExt, baos);
            baos.flush();
            byte[] imageInByte = baos.toByteArray();
            baos.close();
            return imageInByte;
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public static String debug(byte[] bbs) {
        StringBuffer sb = new StringBuffer();
        int counter = 0;
        for (byte b : bbs) {
            sb.append(b);
            sb.append(",");
            if (counter++ % 30 == 0) {
                sb.append("\n");
            }
        }
        return sb.toString();
    }

    public static String getExtension(File f) {
        String ext = null;
        String s = f.getName();
        int i = s.lastIndexOf('.');

        if (i > 0 && i < s.length() - 1) {
            ext = s.substring(i + 1).toLowerCase();
        }
        return ext;
    }

    public static BufferedImage bytesToBufferedImage(byte[] bbs) {
        try {
            ByteArrayInputStream in = new ByteArrayInputStream(bbs);
            MemoryCacheImageInputStream mem = new MemoryCacheImageInputStream(in);
            BufferedImage bImageFromConvert = ImageIO.read(mem);
            return bImageFromConvert;
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public static String arr2str(String[] arr) {
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < arr.length; i++) {
            sb.append(arr[i]);
            if (i < arr.length - 1) {
                sb.append(",");
            }
        }
        return sb.toString();
    }

    public static Object xml2entry(String content) {
        try {
            ByteArrayInputStream bais = new ByteArrayInputStream(content.getBytes("UTF-8"));
            XMLDecoder decoder = new XMLDecoder(bais);
            Object eom = decoder.readObject();
            decoder.close();
            bais.close();
            return eom;
        } catch (Exception ex) {
            logger.log(Level.SEVERE, ex.getMessage(), ex);
            return new Object();
        }
    }

    public static String entry2xml(Object eom) {
        try {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            XMLEncoder encoder = new XMLEncoder(baos) {
                public PersistenceDelegate getPersistenceDelegate(Class<?> type) {
                    if (type == byte[].class)
                        return new ByteArrayPersistenceDelegate();
                    else
                        return super.getPersistenceDelegate(type);
                }
            };
            encoder.writeObject(eom);
            encoder.close();
            baos.close();
            return new String(baos.toByteArray(), "UTF-8");
        } catch (Exception ex) {
            logger.log(Level.SEVERE, ex.getMessage(), ex);
            return "";
        }
    }

    public static Map<String, String> getCookiesMap(HttpServletRequest request) {
        Cookie[] ccs = request.getCookies();
        Map<String, String> m = new HashMap<String, String>();
        if (ccs == null) {
            return m;
        }
        for (Cookie c : ccs) {
            m.put(c.getName(), c.getValue());
        }
        return m;
    }


    public static MediaFile getMediaFile(EntryObjectModel eom, long checksum) {
        if (eom.getPartOfSpeeches() != null) {
            for (PartOfSpeech pos : eom.getPartOfSpeeches()) {
                if (pos.getEntryDefinitions() != null) {
                    for (EntryDefinition entryDefinition : pos.getEntryDefinitions()) {
                        if (entryDefinition.getImages() != null) {
                            for (EntryImage entryImage : entryDefinition.getImages()) {
                                if (entryImage.getChecksum() == checksum) {
                                    return new MediaFile(entryImage.getImage(), entryImage.getExt());
                                }
                            }
                        }
                        if (entryDefinition.getExamples() != null) {
                            for (Example ex : entryDefinition.getExamples()) {
                                if (ex.getChecksum() == checksum) {
                                    return new MediaFile(ex.getAudio(), ex.getExt());
                                }
                            }
                        }
                    }
                }
            }
        }
        return null;
    }
}
