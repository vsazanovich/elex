package com.google.code.elex.shared;

import winstone.Launcher;

import java.io.IOException;
import java.lang.reflect.Field;
import java.net.DatagramSocket;
import java.net.ServerSocket;
import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.*;

/**
 * User: Vitaly Sazanovich
 * Date: 06/11/12
 * Time: 15:35
 * Email: Vitaly.Sazanovich@gmail.com
 */
public class ElexLauncher {

    public static final int MIN_PORT_NUMBER = 1100;
    public static final int MAX_PORT_NUMBER = 49151;

    public static void main(String argv[]) throws IOException {
//        Handler handler = new FileHandler("log.txt", 1000000, 10);
//        handler.setFormatter(new SimpleFormatter());
//        handler.setLevel(Level.ALL);
//        Logger.getLogger("").addHandler(handler);

        try {
            System.setProperty("file.encoding", "UTF-8");
            Field charset = Charset.class.getDeclaredField("defaultCharset");
            charset.setAccessible(true);
            charset.set(null, null);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        List<String> argsL = Arrays.asList(argv);
        Map<String, String> argsM = new HashMap();
        for (String arg : argsL) {
            String key = arg.split("=")[0];
            String value = arg.split("=").length > 1 ? arg.split("=")[1] : "";
            argsM.put(key, value);
        }

        if (argsM.containsKey("-r")) {
            System.setProperty(ElexConstants.REBUILD_INDEX, String.valueOf(true));
        }

        StringBuffer sb = new StringBuffer();
        for (String key : argsM.keySet()) {
            sb.append(key);
            if (!argsM.get(key).equals("")) {
                sb.append("=");
                sb.append(argsM.get(key));
            }
            sb.append(" ");
        }
        boolean isStartAll = !argsM.containsKey("server") && !argsM.containsKey("client");

//        if (argsM.containsKey("server") || isStartAll) {
            Launcher.main(sb.toString().split(" "));
//        }
//        if (argsM.containsKey("client") || isStartAll) {
//            AppMain.main(sb.toString().split(" "));
//        }

    }

    /**
     * Checks to see if a specific port is available.
     *
     * @param port the port to check for availability
     */
    public static boolean available(int port) {
        if (port < MIN_PORT_NUMBER || port > MAX_PORT_NUMBER) {
            throw new IllegalArgumentException("Invalid start port: " + port);
        }

        ServerSocket ss = null;
        DatagramSocket ds = null;
        try {
            ss = new ServerSocket(port);
            ss.setReuseAddress(true);
            ds = new DatagramSocket(port);
            ds.setReuseAddress(true);
            return true;
        } catch (IOException e) {
        } finally {
            if (ds != null) {
                ds.close();
            }

            if (ss != null) {
                try {
                    ss.close();
                } catch (IOException e) {
                    /* should not be thrown */
                }
            }
        }

        return false;
    }
}
