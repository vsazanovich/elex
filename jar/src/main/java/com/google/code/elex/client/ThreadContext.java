package com.google.code.elex.client;

import com.google.code.elex.shared.ElexConstants;

/**
 * User: Vitaly Sazanovich
 * Date: 1/13/13
 * Time: 9:22 PM
 */
public class ThreadContext {
    private String lang = ElexConstants.DEFAULT_LOCALE;

    public String getLang() {
        return lang.toLowerCase();
    }

    public void setLang(String lang) {
        this.lang = lang;
    }
}
