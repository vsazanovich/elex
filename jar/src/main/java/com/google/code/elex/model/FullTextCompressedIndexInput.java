package com.google.code.elex.model;

import org.apache.lucene.store.BufferedIndexInput;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Author: Vitaly Sazanovich
 * Email: Vitaly.Sazanovich@gmail.com
 * Date: 11/28/13
 */
public class FullTextCompressedIndexInput extends BufferedIndexInput {
    private static final Logger logger = Logger.getLogger(FullTextCompressedIndexInput.class.getName());

    private CompressedDictionary compressedDictionary;
    private DictionarySectionDescriptor descriptor;
    private String resDescription;
    private RandomAccessFile raf;

    protected FullTextCompressedIndexInput(CompressedDictionary cd, DictionarySectionDescriptor d, String resourceDescription) throws FileNotFoundException {
        super(resourceDescription);
        this.compressedDictionary = cd;
        this.descriptor = d;
        this.resDescription = resourceDescription;
        this.raf = new RandomAccessFile(cd.getCacheFolder() + File.separator + cd.getCacheFileName(), "r");
    }


    @Override
    protected void readInternal(byte[] b, int offset, int length) throws IOException {
//        System.out.println(this.descriptor.name+":read:"+offset);
        raf.read(b, offset, length);
    }

    @Override
    protected void seekInternal(long pos) throws IOException {
//        System.out.println(this.descriptor.name+":seek:"+pos);
        raf.seek(descriptor.offset + pos);
    }

    @Override
    public void close() throws IOException {
    }

    @Override
    public long length() {
        if (descriptor == null) {
            logger.log(Level.SEVERE, resDescription);
        }
        return descriptor.size;
    }
}