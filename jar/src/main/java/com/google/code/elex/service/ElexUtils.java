package com.google.code.elex.service;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang3.CharEncoding;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.en.EnglishAnalyzer;
import org.apache.lucene.analysis.nl.DutchAnalyzer;
import org.apache.lucene.util.Version;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;

import javax.imageio.ImageIO;
import javax.imageio.stream.MemoryCacheImageInputStream;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.awt.image.BufferedImage;
import java.io.*;
import java.nio.ByteBuffer;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.BitSet;
import java.util.List;
import java.util.Properties;
import java.util.logging.Logger;


/**
 * Author: Vitaly Sazanovich
 * Email: vitaly.sazanovich@gmail.com
 * Date: 05/11/13
 */
public class ElexUtils {
    private static final Logger logger = Logger.getLogger(ElexUtils.class.getName());

    public static String getCacheFileNameByZipFile(File zipFile) throws Exception {
        long lm = zipFile.lastModified();
        String nameHash = DigestUtils.md5Hex(zipFile.getName());
        String filter = lm + "_" + nameHash + "_";
        String fileHash = ElexUtils.createChecksumFromStream(new FileInputStream(zipFile));
        return filter + fileHash;
    }

    public static Document loadXMLFromString(String xml) throws Exception {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        InputSource is = new InputSource(new StringReader(xml));
        return builder.parse(is);
    }

    public static String convertToXml(Document doc) throws TransformerException {
        TransformerFactory tf = TransformerFactory.newInstance();
        Transformer transformer = tf.newTransformer();
        transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
        StringWriter writer = new StringWriter();
        transformer.transform(new DOMSource(doc), new StreamResult(writer));
        String output = writer.getBuffer().toString().replaceAll("\n|\r", "");
        return output;
    }

    public static BufferedImage bytesToBufferedImage(byte[] bbs) {
        try {
            ByteArrayInputStream in = new ByteArrayInputStream(bbs);
            MemoryCacheImageInputStream mem = new MemoryCacheImageInputStream(in);
            BufferedImage bImageFromConvert = ImageIO.read(mem);
            return bImageFromConvert;
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public static byte[] bufferedImageToBytes(String fileExt, BufferedImage bi) {
        try {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            ImageIO.write(bi, fileExt, baos);
            baos.flush();
            byte[] imageInByte = baos.toByteArray();
            baos.close();
            return imageInByte;
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public static String arr2str(String[] arr) {
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < arr.length; i++) {
            sb.append(arr[i]);
            if (i < arr.length - 1) {
                sb.append(",");
            }
        }
        return sb.toString();
    }

    public static String getExtension(File f) {
        String ext = null;
        String s = f.getName();
        int i = s.lastIndexOf('.');

        if (i > 0 && i < s.length() - 1) {
            ext = s.substring(i + 1).toLowerCase();
        }
        return ext;
    }

    public static byte[] serialize(Serializable obj) throws IOException {
        ByteArrayOutputStream b = new ByteArrayOutputStream();
        ObjectOutputStream o = new ObjectOutputStream(b);
        o.writeObject(obj);
        return b.toByteArray();
    }

    public static Serializable deserialize(byte[] bytes) throws IOException, ClassNotFoundException {
        ByteArrayInputStream b = new ByteArrayInputStream(bytes);
        ObjectInputStream o = new ObjectInputStream(b);
        return (Serializable) o.readObject();
    }

    public static byte[] int2bytes(int i) {
        return ByteBuffer.allocate(4).putInt(i).array();
    }

    public static int bytes2int(byte[] bbs) {
        return ByteBuffer.wrap(bbs).getInt();
    }

    public static byte[] longToBytes(long x) {
        ByteBuffer buffer = ByteBuffer.allocate(8);
        buffer.putLong(x);
        return buffer.array();
    }

    public static long bytesToLong(byte[] bytes) {
        ByteBuffer buffer = ByteBuffer.allocate(8);
        buffer.put(bytes);
        buffer.flip();//need flip
        return buffer.getLong();
    }

    public static byte[] createChecksum(byte[] filename) throws Exception {
        InputStream fis = new ByteArrayInputStream(filename);

        byte[] buffer = new byte[1024];
        MessageDigest complete = MessageDigest.getInstance("MD5");
        int numRead;

        do {
            numRead = fis.read(buffer);
            if (numRead > 0) {
                complete.update(buffer, 0, numRead);
            }
        } while (numRead != -1);

        fis.close();
        return complete.digest();
    }

    public static String createChecksumFromStream(InputStream is) throws Exception {
        return DigestUtils.md5Hex(is);
    }


    // see this How-to for a faster way to convert
    // a byte array to a HEX string
    public static String getMD5Checksum(byte[] filename) throws Exception {
        byte[] b = createChecksum(filename);
        String result = "";

        for (int i = 0; i < b.length; i++) {
            result += Integer.toString((b[i] & 0xff) + 0x100, 16).substring(1);
        }
        return result;
    }

    public static BitSet convert(long value) {
        BitSet bits = new BitSet();
        int index = 0;
        while (value != 0L) {
            if (value % 2L != 0) {
                bits.set(index);
            }
            ++index;
            value = value >>> 1;
        }
        return bits;
    }

    public static long convert(BitSet bits) {
        long value = 0L;
        for (int i = 0; i < bits.length(); ++i) {
            value += bits.get(i) ? (1L << i) : 0L;
        }
        return value;
    }

    public static Document string2dom(String xml) throws Exception {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        factory.setNamespaceAware(true);
        DocumentBuilder builder = factory.newDocumentBuilder();
        return builder.parse(new InputSource(new StringReader(xml)));
    }

    public static String dom2string(Document doc) throws Exception {
        TransformerFactory tf = TransformerFactory.newInstance();
        Transformer transformer = tf.newTransformer();
        transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
        StringWriter writer = new StringWriter();
        transformer.transform(new DOMSource(doc), new StreamResult(writer));
        String output = writer.getBuffer().toString().replaceAll("\n|\r", "");
        return output;
    }

    public static String readLine(RandomAccessFile is) throws Exception {
        byte result = (byte) is.read();
        List<Byte> l = new ArrayList<Byte>();
        while (result != -1 && ((char) result) != '\n') {
            l.add(result);
            result = (byte) is.read();
        }
        if (l.isEmpty()) return null;
        byte[] bbs = new byte[l.size()];
        int counter = 0;
        for (Byte b : l) {
            bbs[counter++] = b.byteValue();
        }
        return new String(bbs, CharEncoding.UTF_8);
    }

    public static String getKeyByValue(Properties props, String val) throws IOException {
        for (Object k : props.keySet()) {
            if (props.getProperty((String) k).equals(val)) {
                return (String) k;
            }
        }
        return null;
    }

    public static Analyzer getAnalyzerByLanguage(String property) {
        if (property.equals("nl")) {
            return new DutchAnalyzer(Version.LUCENE_46);
        }
        return new EnglishAnalyzer(Version.LUCENE_46);
    }

    public static byte[] createChecksum(InputStream fis) throws Exception {
//        InputStream fis =  new FileInputStream(filename);

        byte[] buffer = new byte[1024];
        MessageDigest complete = MessageDigest.getInstance("MD5");
        int numRead;

        do {
            numRead = fis.read(buffer);
            if (numRead > 0) {
                complete.update(buffer, 0, numRead);
            }
        } while (numRead != -1);

        fis.close();
        return complete.digest();
    }

    // see this How-to for a faster way to convert
    // a byte array to a HEX string
    public static String getMD5Checksum(InputStream fis) throws Exception {
        byte[] b = createChecksum(fis);
        String result = "";

        for (int i = 0; i < b.length; i++) {
            result += Integer.toString((b[i] & 0xff) + 0x100, 16).substring(1);
        }
        return result;
    }

    public static String stripTags(String entry) {
        entry = entry.replaceAll("\\[[^]]+\\]", "");
        entry = entry.replaceAll("\t|\r|\n", "");
        entry = entry.replaceAll("<[^>]+>", " ");
        entry = entry.replaceAll("\\s\\s", " ");
        entry = entry.replaceAll("  ", " ");
        entry = entry.replaceAll("_", " ");
        return entry;
    }
}
