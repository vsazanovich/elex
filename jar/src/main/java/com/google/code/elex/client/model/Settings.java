package com.google.code.elex.client.model;

import java.io.Serializable;

/**
 * User: Vitaly Sazanovich
 * Date: 14/11/12
 * Time: 16:17
 * Email: Vitaly.Sazanovich@gmail.com
 */
public class Settings implements Serializable {
    private String xsl;
    private String css;

    public String getXsl() {
        return xsl;
    }

    public void setXsl(String xsl) {
        this.xsl = xsl;
    }

    public String getCss() {
        return css;
    }

    public void setCss(String css) {
        this.css = css;
    }
}
