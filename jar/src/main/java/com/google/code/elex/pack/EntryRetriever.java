package com.google.code.elex.pack;

import com.google.code.elex.api.IEntryRetriever;
import com.google.code.elex.model.CompressedDictionary;
import com.google.code.elex.model.DictionarySectionDescriptor;
import com.google.code.elex.service.ElexUtils;
import org.apache.commons.lang3.CharEncoding;

import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

/**
 * Author: Vitaly Sazanovich
 * Email: vitaly.sazanovich@gmail.com
 * Date: 27/11/13
 */
public class EntryRetriever implements IEntryRetriever {
    private static final Logger logger = Logger.getLogger(EntryRetriever.class.getName());

    public static void main(String[] args) throws Exception {
        EntryRetriever entryRetriever = new EntryRetriever(null);
        String entry = entryRetriever.retrieveEntry(new RandomAccessFile("dictionaries/ushakov/Ushakov.dsl", "r"), new RandomAccessFile("dictionaries/ushakov/Ushakov.idx", "r"), 10000);
        System.out.println(entry);
    }

    private CompressedDictionary compressedDictionary;

    public EntryRetriever() {

    }

    public EntryRetriever(CompressedDictionary cd) {
        this.compressedDictionary = cd;
    }

    public String retrieveEntry(RandomAccessFile sourceFile, RandomAccessFile indexFile, int entryNumber) throws Exception {
        if (entryNumber < 0 || entryNumber >= indexFile.length() / 4) {
            return null;
        }
        indexFile.seek((entryNumber - 1) * 4);
        byte[] bbs = new byte[4];
        indexFile.read(bbs);
        int from = ElexUtils.bytes2int(bbs);

        bbs = new byte[4];
        indexFile.read(bbs);
        int to = ElexUtils.bytes2int(bbs);

        sourceFile.seek(from);
        bbs = new byte[to - from];
        sourceFile.read(bbs);
        return new String(bbs, CharEncoding.UTF_8);

    }

    public String retrieveHeader(RandomAccessFile sourceFile) throws IOException {
        StringBuilder sb = new StringBuilder();
        long pointer = sourceFile.getFilePointer();
        String line = readLine(sourceFile);
        while (line.startsWith("#")) {
            pointer = sourceFile.getFilePointer();
            sb.append(line);
            line = readLine(sourceFile);
        }
        sourceFile.seek(pointer);
        return sb.toString();
    }

    public String retrieveEntry(RandomAccessFile sourceFile) throws Exception {
        StringBuilder sb = new StringBuilder();
        long pointer;
        String line = readLine(sourceFile);
        if (line == null) {
            return null;//EOF?
        }
        if (!line.startsWith("\t")) {//header
            pointer = sourceFile.getFilePointer();
            sb.append(line);
            line = readLine(sourceFile);
        } else {
            throw new Exception("should point at entry start");
        }
        while (line != null && line.startsWith("\t")) {
            pointer = sourceFile.getFilePointer();
            sb.append(line);
            line = readLine(sourceFile);
        }
        sourceFile.seek(pointer);
        return sb.toString();
    }

    public String retrieveEntryByOffset(int offset, boolean includeHeadword) throws Exception {
        DictionarySectionDescriptor index = compressedDictionary.getIndexSectionDescriptor();
        DictionarySectionDescriptor content = compressedDictionary.getContentSectionDescriptor();

        if (offset < 0 || offset >= index.size / 4) {
            return null;
        }

        byte[] bbs = new byte[4];

        synchronized (compressedDictionary.inputStream) {
            compressedDictionary.inputStream.seek(index.offset + offset * 4);

            compressedDictionary.inputStream.read(bbs);
            int from = ElexUtils.bytes2int(bbs);

            bbs = new byte[4];
            compressedDictionary.inputStream.read(bbs);
            int to = ElexUtils.bytes2int(bbs);


            compressedDictionary.inputStream.seek(content.offset + from);
            if (to - from < 0 || to - from > 1000000) {
                logger.severe("Entry size exceeds the limits: " + (to - from) + " for offset: " + offset);
                return "";
            }
            bbs = new byte[to - from];
            compressedDictionary.inputStream.read(bbs);
        }
        String res = new String(bbs, CharEncoding.UTF_8);
        if (!includeHeadword) {
            return res.split("\n")[1];
        }

        return res;
    }

    @Override
    public String retrieveHeadword(int offset) throws Exception {
        String entry = retrieveEntryByOffset(offset, true);
        return entry.split("\n")[0].replaceAll("\r", "").trim();
    }


    private String readLine(RandomAccessFile in) throws IOException {
        List<Byte> bbs = new ArrayList<Byte>();
        int res = in.read();
        while (res != -1 && res != '\n') {
            bbs.add((byte) res);
            res = in.read();
        }
        if (res == -1 && bbs.isEmpty()) {
            return null;
        }


        if (res != -1) {
            bbs.add((byte) res);
        }
        byte[] bb = new byte[bbs.size()];
        for (int i = 0; i < bbs.size(); i++) {
            bb[i] = bbs.get(i);
        }
        return new String(bb, CharEncoding.UTF_8);
    }
}
