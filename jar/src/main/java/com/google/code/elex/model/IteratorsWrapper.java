package com.google.code.elex.model;

import com.google.code.elex.api.IPeekIterator;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.text.Collator;
import java.util.HashSet;
import java.util.Set;

/**
 * Author: Vitaly Sazanovich
 * Email: vitaly.sazanovich@gmail.com
 * Date: 05/02/14
 * Time: 16:10
 */
public class IteratorsWrapper implements IPeekIterator {
    private Set<IndexIterator> iterators = new HashSet<IndexIterator>();
    boolean isForward;
    String searchValue;
    Collator collator;

    public IteratorsWrapper(Set<CompressedDictionary> set, String sv, boolean iF, Collator c) throws Exception {
        for (CompressedDictionary cd : set) {
            IndexIterator si = new IndexIterator(cd, sv, iF);
            iterators.add(si);
            isForward = iF;
            searchValue = sv;
            collator = c;
        }
    }

    @Override
    public boolean hasNext() {
        for (IndexIterator ii : iterators) {
            if (ii.hasNext()) {
                return true;
            }
        }
        return false;
    }

    /**
     * @return
     */
    @Override
    public String next() {
        String nextCandidate = peek();
        //moving all required iterators
        for (IndexIterator ii : iterators) {
            String peek = ii.peek();
            if (peek == null) {
                continue;
            }
            if (isForward) {
                if (collator.compare(peek, nextCandidate) <= 0) {
                    ii.next();
                }
            } else {
                if (collator.compare(peek, nextCandidate) >= 0) {
                    ii.next();
                }
            }
        }
        return nextCandidate;
    }

    @Override
    public String peek() {
        String nextCandidate = null;
        //find the best candidate
        for (IndexIterator ii : iterators) {
            String peek = ii.peek();

            if (peek == null) {
                continue;
            }

            if (peek.equals(searchValue)) {
                nextCandidate = peek;
                break;
            }

            if (ii.hasNext()) {
                if (isForward) {
                    if (nextCandidate == null || collator.compare(peek, nextCandidate) < 0) {
                        nextCandidate = peek;
                    }
                } else {
                    if (nextCandidate == null || collator.compare(peek, nextCandidate) > 0) {
                        nextCandidate = peek;
                    }
                }
            }
        }
        return nextCandidate;
    }

    @Override
    public void remove() {
        throw new NotImplementedException();
    }
}
