package com.google.code.elex.shared;

/**
 * User: Vitaly Sazanovich
 * Date: 22/11/12
 * Time: 16:15
 * Email: Vitaly.Sazanovich@gmail.com
 */
public class ElexConstants {
    public static String VERSION = "0.0.1";

    public static final String REBUILD_INDEX = "REBUILD_INDEX";

    public static final String ENTRY_ID_PARAM_NAME = "ei";
    public static final String IS_SOURCE_PARAM_NAME = "s";
    public static final String DATA_PARAM_NAME = "data";
    public static final String HEADWORD_PARAM_NAME = "hw";
    public static final String CHECKSUM_PARAM_NAME = "cs";
    public static final String LOCALE_PARAM_NAME = "l";
    public static final String REMOVE_PARAM_NAME = "r";
    public static final String ACTION_PARAM_NAME = "a";
    public static final String RND_PARAM_NAME = "rnd";
    public static final String PAGE_SIZE_PARAM_NAME = "ps";
    public static final String LOCAL_INDEX_PARAM_NAME = "li";
    public static final String GLOBAL_INDEX_PARAM_NAME = "gi";
    public static final String SEARCH_VALUE_PARAM_NAME = "sv";

    public static final String DEFAULT_LOCALE = "en";
    public static final int DEFAULT_PAGE_SIZE = 1;

    public static final String ERROR_HEADWORD_EXISTS = "ERROR_HEADWORD_EXISTS";
    public static final String ERROR_HEADWORD_INVALID = "ERROR_HEADWORD_INVALID";
    public static final String ERROR_NO_ID = "ERROR_NO_ID";
    public static final String ERROR_ID_NOT_EMPTY = "ERROR_ID_NOT_EMPTY";
    public static final String ERROR_CONTENT_EMPTY = "ERROR_CONTENT_EMPTY";
    public static final String ERROR_EMPTY_LOCALE = "ERROR_EMPTY_LOCALE";
    public static final String ERROR_COULD_NOT_REMOVE = "ERROR_COULD_NOT_REMOVE";

    public static final String UPDATE_ACTION = "u";
    public static final String NEW_ACTION = "n";

    public static String MEDIA_CURRENT_DIRECTORY;


}
