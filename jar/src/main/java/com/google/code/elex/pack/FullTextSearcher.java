package com.google.code.elex.pack;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.en.EnglishAnalyzer;
import org.apache.lucene.analysis.ru.RussianAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopScoreDocCollector;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.util.Version;

import java.io.File;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.List;

/**
 * Author: Vitaly Sazanovich
 * Email: vitaly.sazanovich@gmail.com
 * Date: 28/11/13
 */
public class FullTextSearcher {

    public static void main(String[] args) throws Exception {
        FullTextSearcher fullTextSearcher = new FullTextSearcher();
        Directory directory = FSDirectory.open(new File("dictionaries/ushakov"));
        List<Document> l = fullTextSearcher.search(directory, "плакал");

        EntryRetriever er = new EntryRetriever();
        RandomAccessFile source = new RandomAccessFile("dictionaries/ushakov/Ushakov.dsl", "r");
        RandomAccessFile index = new RandomAccessFile("dictionaries/ushakov/Ushakov.idx", "r");

        for (Document doc : l) {
            int offset = Integer.parseInt(doc.get("offset"));
            String entry = er.retrieveEntry(source, index, offset);
            System.out.println(entry);
        }
    }

    public List<Document> search(Directory directory, String querystr) throws Exception {
        Analyzer analyzer = new RussianAnalyzer(Version.LUCENE_46);
        Query q = new QueryParser(Version.LUCENE_46, "content", analyzer).parse(querystr);

        int hitsPerPage = 10;

        IndexReader reader = DirectoryReader.open(directory);
        IndexSearcher searcher = new IndexSearcher(reader);
        TopScoreDocCollector collector = TopScoreDocCollector.create(hitsPerPage, true);
        searcher.search(q, collector);
        ScoreDoc[] hits = collector.topDocs().scoreDocs;
        List<Document> l = new ArrayList<Document>();
        for (int i = 0; i < hits.length; ++i) {
            int docId = hits[i].doc;
            Document d = searcher.doc(docId);
            l.add(d);
        }
        return l;
    }

}
