package com.google.code.elex.pack;

import com.google.code.elex.service.ElexUtils;
import com.google.common.io.CountingInputStream;
import org.apache.commons.lang3.CharEncoding;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Scans dictionary source file and writes array of ints which represent byte offsets to entries.
 * <p/>
 * Author: Vitaly Sazanovich
 * Email: vitaly.sazanovich@gmail.com
 * Date: 27/11/13
 */
public class OffsetIndexer {

    public static void main(String[] args) throws Exception {
        if (args.length != 2) {
            throw new Exception("2 args expected");
        }
        OffsetIndexer offsetIndexer = new OffsetIndexer();
        offsetIndexer.indexDsl(args[0], args[1]);
//        offsetIndexer.indexDsl("dictionaries/ushakov/Ushakov.dsl", "dictionaries/ushakov/Ushakov.idx");
    }

    public void indexDsl(String inputFileName, String outputFileName) {
        CountingInputStream in = null;
        FileOutputStream out = null;
        try {
            in = new CountingInputStream(new FileInputStream(inputFileName));
            out = new FileOutputStream(outputFileName);

            String str = readLine(in);

            while (str.startsWith("#")) {
                str = readLine(in);
            }

            int counter = 0;
            while (str != null) {
                if (!str.startsWith("\t")) {//headword found
                    int offset = (int) in.getCount() - str.getBytes(CharEncoding.UTF_8).length;
                    out.write(ElexUtils.int2bytes(offset));
                    ++counter;
                    if (counter % 1000 == 0) {
                        System.out.println(counter);
                    }
//
                }
                str = readLine(in);
            }
            //last long should be equal to the size of the input stream
            int offset = (int) in.getCount();
            out.write(ElexUtils.int2bytes(offset));
//            System.out.println(offset);


        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (in != null) {
                try {
                    in.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (out != null) {
                try {
                    out.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private String readLine(CountingInputStream in) throws IOException {
        List<Byte> bbs = new ArrayList<Byte>();
        int res = in.read();
        while (res != -1 && res != '\n') {
            bbs.add((byte) res);
            res = in.read();
        }
        if (res == -1) {
            return null;
        }
        bbs.add((byte) res);
        byte[] bb = new byte[bbs.size()];
        for (int i = 0; i < bbs.size(); i++) {
            bb[i] = bbs.get(i);
        }
        return new String(bb, CharEncoding.UTF_8);
    }
}
