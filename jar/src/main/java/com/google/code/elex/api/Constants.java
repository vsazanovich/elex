package com.google.code.elex.api;


import java.io.IOException;
import java.util.Properties;

/**
 * Author: Vitaly Sazanovich
 * Email: vitaly.sazanovich@gmail.com
 * Date: 05/11/13
 */
public class Constants {
    public static final String DICTIONARY_NAME = "name";
    public static final String DICTIONARY_ID = "DICTIONARY_ID";
    public static final String DICTIONARY_SOURCE_LANGUAGE = "index.language";
    public static final String DICTIONARY_NAME_SHORT = "name.short";
    public static final String DICTIONARY_TARGET_LANGUAGE = "contents.language";
    public static final String DICTIONARY_DISPLAY_HEADWORD = "display.headword";
    public static final String DICTIONARY_CONTENT_REPLACEMENTS = "content.replacements";
    public static final String DICTIONARY_FILE_NAME = "DICTIONARY_FILE_NAME";

    public static final String EXCEPTION_ALREADY_EXISTS = "EXCEPTION_ALREADY_EXISTS";
    public static final String EXCEPTION_NOT_EXISTS = "EXCEPTION_NOT_EXISTS";
    public static final String EXCEPTION_EMPTY = "EXCEPTION_EMPTY";
    public static final String EXCEPTION_DATA_DISCREPANCY = "EXCEPTION_DATA_DISCREPANCY";

    public static final String MODE = "MODE";
    public static final String MODE_TEST = "MODE_TEST";
    public static final String MODE_PROD = "MODE_PROD";

    public static final String DEFAULT_LOCALE = "en";

    public static final String UPDATE_ACTION = "u";
    public static final String NEW_ACTION = "n";

    public static String MEDIA_CURRENT_DIRECTORY;

    //do not change the order as it matters, maximum 64 bits, long is used to storing availability
    public static final int SECTION_CONTENT = 0;
    public static final int SECTION_CONTENT_OFFSETS = 1;
    public static final int SECTION_ICON = 2;
    public static final int SECTION_ANNOTATIONS = 3;

    //index view actions
    public static final String PAGE_FORWARD_NAVIGATION_ACTION = "pf";
    public static final String PAGE_BACKWARD_NAVIGATION_ACTION = "pb";

    public static final String FAST_FORWARD_NAVIGATION_ACTION = "ff";
    public static final String FAST_BACKWARD_NAVIGATION_ACTION = "fb";

    public static final String TO_START_NAVIGATION_ACTION = "ts";
    public static final String TO_END_NAVIGATION_ACTION = "te";

    public static final String SEARCH_NAVIGATION_ACTION = "s";
    public static final String SEARCH_BY_INDEX_NAVIGATION_ACTION = "si";

    public static Properties LANGS = null;
    public static Properties ELEX_PROPS = null;


    static {
        try {
            LANGS = new Properties();
            ELEX_PROPS = new Properties();
            Constants constants = new Constants();
            LANGS.load(constants.getClass().getClassLoader().getResourceAsStream("languages.properties"));
            ELEX_PROPS.load(constants.getClass().getClassLoader().getResourceAsStream("elex.properties"));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

}
