package com.google.code.elex.web;

import com.google.code.elex.api.Constants;
import com.google.code.elex.model.CompressedDictionary;
import com.google.code.elex.service.DictionariesPool;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Set;

/**
 * Author: Vitaly Sazanovich
 * Email: vitaly.sazanovich@gmail.com
 * Date: 20/11/13
 */
public class ResourceServlet extends ElexServlet {

    protected void doGet(HttpServletRequest request,
                         HttpServletResponse response) throws ServletException, IOException {

        try {


            String action = request.getParameter("a");
            if (action == null) {
                throw new Exception(Constants.EXCEPTION_EMPTY);
            }

            Set<CompressedDictionary> set = getDics(request);

            if (action.equals("icon")) {
                Integer id = Integer.parseInt(request.getParameter("id"));
                CompressedDictionary ed = DictionariesPool.getInstance().getDictionaryById(id);
                byte[] icon = ed.getSectionBytes("^icon.png$");
                String contentType = "image/png";
                response.setContentType(contentType);
                response.getOutputStream().write(icon);

            } else if (action.equals("css")) {
                DictionariesPool.getInstance().getDictionariesResourcesByType(response.getOutputStream(), set, ".*\\.css$");
                String contentType = "text/css";
                response.setContentType(contentType);
            } else if (action.equals("js")) {
                DictionariesPool.getInstance().getDictionariesResourcesByType(response.getOutputStream(), set, ".*\\.js$");
                String contentType = "application/javascript";
                response.setContentType(contentType);
            } else if (action.equals("aux")) {
                String dicShortName = request.getParameter("dicName");
                String resId = request.getParameter("resId");
                DictionariesPool.getInstance().getDictionaryByShortName(dicShortName).getResource(response.getOutputStream(),resId);
                String contentType = "audio/mpeg";
                response.setContentType(contentType);

            } else {
                throw new Exception(Constants.EXCEPTION_NOT_EXISTS);
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            response.getOutputStream().flush();
            response.getOutputStream().close();
        }
    }

}
