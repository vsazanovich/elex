package com.google.code.elex.client.model;

/**
 * User: Vitaly Sazanovich
 * Date: 12/30/12
 * Time: 12:35 AM
 */
public class MediaFile {
    public byte[] file;
    public String extension;

    public MediaFile(byte[] f, String e) {
        this.file = f;
        this.extension = e;
    }
}
