package com.google.code.elex.client.model;

import java.io.Serializable;

/**
 * User: Vitaly Sazanovich
 * Date: 9/19/12
 * Time: 11:05 PM
 */
public class DictionaryEntry implements Serializable {
    private Integer id;
    private int globalPositionInIndex;//starts from 1
    private String headword;
    private String content;
    private String contentHTML;
    private Object entryObjectModel;

    public DictionaryEntry(){

    }

    public Object getEntryObjectModel() {
        return entryObjectModel;
    }

    public void setEntryObjectModel(Object entryObjectModel) {
        this.entryObjectModel = entryObjectModel;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("{\"id\":");
        sb.append(id);
        sb.append(",\"name\": \"");
        sb.append(headword);
        sb.append("\"}");
        return sb.toString();
    }

    public int getGlobalPositionInIndex() {
        return globalPositionInIndex;
    }

    public void setGlobalPositionInIndex(int globalPositionInIndex) {
        this.globalPositionInIndex = globalPositionInIndex;
    }


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getHeadword() {
        return headword;
    }

    public void setHeadword(String headword) {
        this.headword = headword;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getContentHTML() {
        return contentHTML;
    }

    public void setContentHTML(String contentHTML) {
        this.contentHTML = contentHTML;
    }
}
