package com.google.code.elex.model;

import org.apache.lucene.store.*;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.io.IOException;
import java.util.Collection;
import java.util.SortedSet;

/**
 * Author: Vitaly Sazanovich
 * Email: Vitaly.Sazanovich@gmail.com
 * Date: 11/28/13
 */
public class FullTextCompressedDirectory extends Directory {
    private CompressedDictionary compressedDictionary;

    public FullTextCompressedDirectory(CompressedDictionary cd) {
        this.compressedDictionary = cd;
    }

    @Override
    public String[] listAll() throws IOException {
        SortedSet<DictionarySectionDescriptor> descriptors = compressedDictionary.getDictionarySectionDescriptors();
        String[] all = new String[descriptors.size()];
        int counter = 0;
        for (DictionarySectionDescriptor d : descriptors) {
            all[counter++] = d.name;
        }
        return all;
    }

    @Override
    public boolean fileExists(String s) throws IOException {
        SortedSet<DictionarySectionDescriptor> descriptors = compressedDictionary.getDictionarySectionDescriptors();
        for (DictionarySectionDescriptor d : descriptors) {
            if (d.name.equals(s)) return true;
        }
        return false;
    }

    @Override
    public void deleteFile(String s) throws IOException {
        throw new NotImplementedException();
    }

    @Override
    public long fileLength(String s) throws IOException {
        SortedSet<DictionarySectionDescriptor> descriptors = compressedDictionary.getDictionarySectionDescriptors();
        for (DictionarySectionDescriptor d : descriptors) {
            if (d.name.equals(s)) return d.size;
        }
        throw new IOException("File not found:" + s);
    }

    @Override
    public IndexOutput createOutput(String s, IOContext ioContext) throws IOException {
        throw new NotImplementedException();
    }

    @Override
    public void sync(Collection<String> strings) throws IOException {
        throw new NotImplementedException();
    }

    @Override
    public IndexInput openInput(String s, IOContext ioContext) throws IOException {
        DictionarySectionDescriptor descriptor = compressedDictionary.find("^"+s+"$");
        return new FullTextCompressedIndexInput(compressedDictionary, descriptor, s);
    }

    @Override
    public Lock makeLock(String s) {
        throw new NotImplementedException();
    }

    @Override
    public void clearLock(String s) throws IOException {
        throw new NotImplementedException();
    }

    @Override
    public void close() throws IOException {
        throw new NotImplementedException();
    }

    @Override
    public void setLockFactory(LockFactory lockFactory) throws IOException {
        throw new NotImplementedException();
    }

    @Override
    public LockFactory getLockFactory() {
        throw new NotImplementedException();
    }
}
