package com.google.code.elex.client.entry;


import java.io.Serializable;

/**
 * User: Vitaly Sazanovich
 * Date: 27/11/12
 * Time: 19:21
 * Email: Vitaly.Sazanovich@gmail.com
 */
public class Grammar implements Serializable {
    private String posKey;
    private String genderKey;
    private String plural;
    private String comparative;
    private String superlative;
    private String verbType;
    private String verleden;
    private String helpVerb;
    private String voltooid;
    private Boolean alleenAttributief;
    private Boolean alleenPredikatief;
    private Boolean ookAbsoluut;
    private Boolean alleenEnkel;
    private Boolean alleenMeer;

    private String vnwKey;

    public boolean isEmpty() {
        if (genderKey != null && !genderKey.trim().equals("")) return false;
        if (plural != null && !plural.trim().equals("")) return false;
        if (comparative != null && !comparative.trim().equals("")) return false;
        if (superlative != null && !superlative.trim().equals("")) return false;
        if (verleden != null && !verleden.trim().equals("")) return false;
        if (voltooid != null && !voltooid.trim().equals("")) return false;
        if (alleenAttributief != null && !alleenAttributief.booleanValue()==false) return false;
        if (alleenPredikatief != null && !alleenPredikatief.booleanValue()==false) return false;
        if (ookAbsoluut != null && !ookAbsoluut.booleanValue()==false) return false;
        if (alleenEnkel != null && !alleenEnkel.booleanValue()==false) return false;
        if (alleenMeer != null && !alleenMeer.booleanValue()==false) return false;
        return true;
    }

    public String getVnwKey() {
        return vnwKey;
    }

    public void setVnwKey(String vnwKey) {
        this.vnwKey = vnwKey;
    }

    public Boolean getAlleenEnkel() {
        return alleenEnkel;
    }

    public void setAlleenEnkel(Boolean alleenEnkel) {
        this.alleenEnkel = alleenEnkel;
    }

    public Boolean getAlleenMeer() {
        return alleenMeer;
    }

    public void setAlleenMeer(Boolean alleenMeer) {
        this.alleenMeer = alleenMeer;
    }

    public Boolean getOokAbsoluut() {
        return ookAbsoluut;
    }

    public void setOokAbsoluut(Boolean ookAbsoluut) {
        this.ookAbsoluut = ookAbsoluut;
    }

    public Boolean getAlleenPredikatief() {
        return alleenPredikatief;
    }

    public void setAlleenPredikatief(Boolean alleenPredikatief) {
        this.alleenPredikatief = alleenPredikatief;
    }

    public Boolean getAlleenAttributief() {
        return alleenAttributief;
    }

    public void setAlleenAttributief(Boolean alleenAttributief) {
        this.alleenAttributief = alleenAttributief;
    }

    public String getVerbType() {
        return verbType;
    }

    public void setVerbType(String verbType) {
        this.verbType = verbType;
    }

    public String getVerleden() {
        return verleden;
    }

    public void setVerleden(String verleden) {
        this.verleden = verleden;
    }

    public String getHelpVerb() {
        return helpVerb;
    }

    public void setHelpVerb(String helpVerb) {
        this.helpVerb = helpVerb;
    }

    public String getVoltooid() {
        return voltooid;
    }

    public void setVoltooid(String voltooid) {
        this.voltooid = voltooid;
    }

    public String getComparative() {
        return comparative;
    }

    public void setComparative(String comparative) {
        this.comparative = comparative;
    }

    public String getSuperlative() {
        return superlative;
    }

    public void setSuperlative(String superlative) {
        this.superlative = superlative;
    }

    public String getPlural() {
        return plural;
    }

    public void setPlural(String plural) {
        this.plural = plural;
    }

    public String getGenderKey() {
        return genderKey;
    }

    public void setGenderKey(String genderKey) {
        this.genderKey = genderKey;
    }

    public String getPosKey() {
        return posKey;
    }

    public void setPosKey(String posKey) {
        this.posKey = posKey;
    }
}
