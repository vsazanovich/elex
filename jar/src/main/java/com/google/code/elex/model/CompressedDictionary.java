package com.google.code.elex.model;

import com.google.code.elex.api.Constants;
import com.google.code.elex.api.IEntryRetriever;
import com.google.code.elex.pack.DslPackager;
import com.google.code.elex.pack.EntryRetriever;
import com.google.code.elex.service.ElexUtils;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopScoreDocCollector;
import org.apache.lucene.search.highlight.Highlighter;
import org.apache.lucene.search.highlight.QueryScorer;
import org.apache.lucene.search.highlight.SimpleHTMLFormatter;
import org.apache.lucene.search.highlight.TextFragment;
import org.apache.lucene.util.Version;

import java.io.*;
import java.text.Collator;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Author: Vitaly Sazanovich
 * Email: vitaly.sazanovich@gmail.com
 * Date: 26/11/13
 */
public class CompressedDictionary {
    private static final Logger logger = Logger.getLogger(CompressedDictionary.class.getName());
    private static final long serialVersionUID = 1L;
    private Properties props;
    //    public RandomAccessGZip.Index inputStream;
    public RandomAccessFile inputStream;
    private SortedSet<DictionarySectionDescriptor> dictionarySectionDescriptors;
    private FullTextCompressedDirectory fullTextCompressedDirectory;
    private DictionarySectionDescriptor indexSectionDescriptor;
    private DictionarySectionDescriptor propsSectionDescriptor;
    private DictionarySectionDescriptor contentSectionDescriptor;
    public IEntryRetriever entryRetriever;
    protected int size;
    private String cacheFileName;
    private Collator collator;
    private String cacheFolder;

    private Set<ResourcesPackage> resourcesPackages = new HashSet<ResourcesPackage>();

    public CompressedDictionary(Collator c) {
        this.collator = c;
    }


    public CompressedDictionary(String cf, String path) throws Exception {
        this.cacheFolder = cf;
        initStream(path);

        //reading the header
        inputStream.seek(inputStream.length() - 4);
        int headerSize = readInt();
        logger.log(Level.FINE, String.valueOf(headerSize));
        byte[] bbs = new byte[headerSize];
        inputStream.seek(inputStream.length() - 4 - headerSize);
        inputStream.read(bbs);
        DictionarySectionDescriptor[] descs = (DictionarySectionDescriptor[]) ElexUtils.deserialize(bbs);
        dictionarySectionDescriptors = new TreeSet<DictionarySectionDescriptor>(Arrays.asList(descs));

        propsSectionDescriptor = find("^props.xml$");
        contentSectionDescriptor = find(".*\\.dsl$|.*\\.xhtml$");
        indexSectionDescriptor = find(".*\\.idx$");


        //reading properties
        props = new Properties();
        if (propsSectionDescriptor != null) {
            byte[] propBbs = new byte[propsSectionDescriptor.size];
            inputStream.seek(propsSectionDescriptor.offset);
            inputStream.read(propBbs);
            props.loadFromXML(new ByteArrayInputStream(propBbs));
            String sl = props.getProperty(Constants.DICTIONARY_SOURCE_LANGUAGE);
            String tl = props.getProperty(Constants.DICTIONARY_TARGET_LANGUAGE);
            props.put(Constants.DICTIONARY_SOURCE_LANGUAGE, Constants.LANGS.containsValue(sl) ? ElexUtils.getKeyByValue(Constants.LANGS, sl) : sl);
            props.put(Constants.DICTIONARY_TARGET_LANGUAGE, Constants.LANGS.containsValue(tl) ? ElexUtils.getKeyByValue(Constants.LANGS, tl) : tl);
            collator = Collator.getInstance(new Locale(props.getProperty(Constants.DICTIONARY_SOURCE_LANGUAGE)));
        }

        fullTextCompressedDirectory = new FullTextCompressedDirectory(this);
        entryRetriever = new EntryRetriever(this);

        size = indexSectionDescriptor.size / 4 - 1;//the last entry is actually the offset to the end
    }

    private void initStream(String path) throws Exception {
//        File zipFile = new File(path);
//        long lm = zipFile.lastModified();
//        String nameHash = DigestUtils.md5Hex(zipFile.getName());
//        String filter = lm + "_" + nameHash + "_";
        cacheFileName = ElexUtils.getCacheFileNameByZipFile(new File(path));
        String filter = cacheFileName.substring(0, cacheFileName.lastIndexOf("_") + 1);

        File cacheFolderF = new File(cacheFolder + File.separator);
        if (!cacheFolderF.exists()) {
            cacheFolderF.mkdirs();
        }
        File[] cacheFiles = cacheFolderF.listFiles();
        for (File f : cacheFiles) {
            if (f.getName().startsWith(filter)) {
                inputStream = new RandomAccessFile(f.getAbsolutePath(), "r");
                cacheFileName = f.getName();
                return;
            }
        }
        //otherwise create this cache file
        String fileName = cacheFolder + File.separator + cacheFileName;
        DslPackager.unpackageForCache(path, fileName);
        inputStream = new RandomAccessFile(fileName, "r");
    }

    public String getCacheFileName() {
        return cacheFileName;
    }


    public DictionarySectionDescriptor find(String pattern) {
        for (DictionarySectionDescriptor descriptor : dictionarySectionDescriptors) {
            if (descriptor.name.matches(pattern)) {
                return descriptor;
            }
        }
        return null;
    }

    public FullTextCompressedDirectory getDirectory() {
        return fullTextCompressedDirectory;
    }

    public String getCacheFolder() {
        return cacheFolder;
    }

//    public int findInsertionPoint(String key, boolean isSearch) throws Exception {
//        int insertionPoint = findInsertionPoint(key);
//        return isSearch ? Math.abs(insertionPoint) : insertionPoint;
//    }

    /**
     * Using binary search
     *
     * @param key
     * @return
     */

    public int findInsertionPoint(String key) throws Exception {
        int low = 0;
        int high = size - 1;

        while (low <= high) {
            int mid = (low + high) >>> 1;
            String midVal = entryRetriever.retrieveHeadword(mid);

            if (collator.compare(midVal, key) < 0)
                low = mid + 1;
            else if (collator.compare(midVal, key) > 0)
                high = mid - 1;
            else
                return mid; // key found
        }
        return -(low + 1);  // key not found.
    }

    /**
     * Should be used for debugging purposes only
     *
     * @param name
     * @return
     * @throws Exception
     */
    public byte[] getSectionBytes(String name) throws Exception {
        DictionarySectionDescriptor section = find(name);
        if (section == null) {
            return null;
        }
        byte[] bbs = new byte[section.size];
        inputStream.seek(section.offset);
        inputStream.read(bbs);
        return bbs;
    }

    public byte[] getSectionBytes(DictionarySectionDescriptor section) throws Exception {
        inputStream.seek(section.offset);
        byte[] bbs = new byte[section.size];
        inputStream.read(bbs);
        return bbs;
    }

    public synchronized void writeSectionBytes(OutputStream os, DictionarySectionDescriptor section) throws Exception {
        synchronized (inputStream) {
            inputStream.seek(section.offset);
            BufferedOutputStream fos = new BufferedOutputStream(os);
            int count = section.size;
            while (count > 0) {
                fos.write(inputStream.read());
                --count;
            }
            fos.flush();
        }
    }


    private int readInt() throws IOException {
        byte[] bbs = new byte[4];
        inputStream.read(bbs);
        return ElexUtils.bytes2int(bbs);
    }

    public void close() {
        try {
            inputStream.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }


    public Properties getProperties() {
        return props;
    }

    public String retrieveRawEntry(int offset) throws Exception {
        return entryRetriever.retrieveEntryByOffset(offset, true);
    }

    public String retrieveEntry(int offset) throws Exception {
        String wholeEntry = entryRetriever.retrieveEntryByOffset(offset, true);
        String hw = wholeEntry.split("\n")[0];
        String entry = wholeEntry.substring(hw.length(), wholeEntry.length() - 1);
        if (displayHeadword()) {
            entry = hw + "\n" + entry;
        }

        int counter = 1;
        boolean hasNext = props.containsKey(Constants.DICTIONARY_CONTENT_REPLACEMENTS + counter);
        while (hasNext) {
            String change = props.getProperty(Constants.DICTIONARY_CONTENT_REPLACEMENTS + counter);
            String newVal = props.getProperty(Constants.DICTIONARY_CONTENT_REPLACEMENTS + (counter + 1));
            entry = entry.replaceAll(change, newVal);
            counter += 2;
            hasNext = props.containsKey(Constants.DICTIONARY_CONTENT_REPLACEMENTS + counter);
        }

        return entry;
    }

    private boolean displayHeadword() {
        if (props.containsKey(Constants.DICTIONARY_DISPLAY_HEADWORD)) {
            return Boolean.parseBoolean(props.getProperty(Constants.DICTIONARY_DISPLAY_HEADWORD));
        } else {
            return false;
        }
    }

    public String dsl2html(String s) {
        s = s.replaceFirst("^([^\n]+)\n", "<span class=\"hw\">$1</span>");
        s = s.replaceAll("\n|\t|\r", "");
        s = s.replaceAll("\\[m1\\]", "<span class=\"m1\">");
        s = s.replaceAll("\\[trn\\]", "<span class=\"trn\">");
        s = s.replaceAll("\\[p\\]", "<span class=\"p\">");
        s = s.replaceAll("\\[c\\]", "<span class=\"c\">");
        s = s.replaceAll("\\[u\\]", "<span class=\"u\">");
        s = s.replaceAll("\\[i\\]", "<span class=\"i\">");
        s = s.replaceAll("\\[ref\\]", "<span class=\"ref\">");
        s = s.replaceAll("\\[b\\]", "<span class=\"b\">");
        s = s.replaceAll("\\[\\!trs\\]", "<span class=\"trs\">");
        s = s.replaceAll("\\[\\*\\]", "<span class=\"sec\">");

        s = s.replaceAll("\\[/m\\]", "</span>");
        s = s.replaceAll("\\[/trn\\]", "</span>");
        s = s.replaceAll("\\[/p\\]", "</span>");
        s = s.replaceAll("\\[/c\\]", "</span>");
        s = s.replaceAll("\\[/u\\]", "</span>");
        s = s.replaceAll("\\[/i\\]", "</span>");
        s = s.replaceAll("\\[/ref\\]", "</span>");
        s = s.replaceAll("\\[/b\\]", "</span>");
        s = s.replaceAll("\\[/\\!trs\\]", "</span>");
        s = s.replaceAll("\\[/\\*\\]", "</span>");
        return s;
    }

    public String retrieveEntry(String hw) throws Exception {
        int offset = findInsertionPoint(hw);
        if (offset < 0) {
            return null;
        }
        return retrieveEntry(offset);
    }

    public DictionarySectionDescriptor getIndexSectionDescriptor() {
        return indexSectionDescriptor;
    }

    public DictionarySectionDescriptor getContentSectionDescriptor() {
        return contentSectionDescriptor;
    }

    public SortedSet<DictionarySectionDescriptor> getDictionarySectionDescriptors() {
        return dictionarySectionDescriptors;
    }

    public int getSize() {
        return size;
    }

    public String getHeadwordAtPercent(int percent) throws Exception {
        int offset = (size - 1) * percent / 100;
        return entryRetriever.retrieveHeadword(offset);
    }

    public String getHeadwordAtOffset(int offset) throws Exception {
        return entryRetriever.retrieveHeadword(offset);
    }

    public void getResource(OutputStream os, String resId) throws Exception {
        for (ResourcesPackage resourcesPackage : resourcesPackages) {
            resourcesPackage.getResourceByName(os, resId);
            return;
        }
    }

    public void addResource(ResourcesPackage resourcesPackage) {
        resourcesPackages.add(resourcesPackage);
    }

    public Map<? extends Float, ? extends List<DictionarySearchResult>> search(String searchValue) throws Exception {
        SortedMap<Float, List<DictionarySearchResult>> sm = new TreeMap<Float, List<DictionarySearchResult>>();
        Analyzer analyzer = ElexUtils.getAnalyzerByLanguage(getProperties().getProperty(Constants.DICTIONARY_TARGET_LANGUAGE));
        Query q = new QueryParser(Version.LUCENE_46, "content", analyzer).parse(searchValue);
        IndexReader reader = DirectoryReader.open(fullTextCompressedDirectory);
        IndexSearcher searcher = new IndexSearcher(reader);
        TopScoreDocCollector collector = TopScoreDocCollector.create(100, true);
        searcher.search(q, collector);
        ScoreDoc[] hits = collector.topDocs().scoreDocs;
        SimpleHTMLFormatter formatter = new SimpleHTMLFormatter();
        Highlighter highlighter = new Highlighter(formatter, new QueryScorer(q));

        for (int i = 0; i < hits.length; ++i) {
            int docId = hits[i].doc;
            Document d = searcher.doc(docId);
            DictionarySearchResult dsr = new DictionarySearchResult();
            dsr.dictionaryId = ((Integer) getProperties().get(Constants.DICTIONARY_ID));
            dsr.score = hits[i].score;
            dsr.headword = d.get("headword");

            int offset = findInsertionPoint(dsr.headword);
            String entry = entryRetriever.retrieveEntryByOffset(offset, false);
            entry = ElexUtils.stripTags(entry);
            TokenStream tokenStream = analyzer.tokenStream(null, new StringReader(entry));
            TextFragment[] fragments = highlighter.getBestTextFragments(tokenStream, entry, false, 5);
            StringBuilder sb = new StringBuilder();
            boolean isFirst = true;
            for (TextFragment fragment : fragments) {
                if (fragment.getScore() > 0) {
                    if (!isFirst) {
                        sb.append(" ... ");
                    }
                    sb.append(fragment.toString());
                    isFirst = false;
                }
            }

            dsr.text = sb.toString();
            List<DictionarySearchResult> l = sm.get(dsr.score);
            if (l == null) {
                l = new ArrayList<DictionarySearchResult>();
            }
            l.add(dsr);

            sm.put(dsr.score, l);
        }
        return sm;
    }
}
