package com.google.code.elex.web;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.CharEncoding;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;

/**
 * Author: Vitaly Sazanovich
 * Email: Vitaly.Sazanovich@gmail.com
 * Date: 2/11/14
 */
public class HtmlServlet extends ElexServlet {

    protected void doGet(HttpServletRequest request,
                         HttpServletResponse response) throws ServletException, IOException {
        OutputStreamWriter osw = new OutputStreamWriter(response.getOutputStream(), CharEncoding.UTF_8);
        PrintWriter out = new PrintWriter(osw, true);
        try {
            request.setCharacterEncoding(CharEncoding.UTF_8);
            response.setCharacterEncoding(CharEncoding.UTF_8);
            response.setContentType("text/html; charset=UTF-8");
            InputStream is = getClass().getClassLoader().getResourceAsStream("index.html");
            byte[] bbs = IOUtils.toByteArray(is);

            String s = new String(bbs, CharEncoding.UTF_8);
            s = s.replaceAll("\\$\\{TIMESTAMP\\}", String.valueOf(System.currentTimeMillis()));
            is.close();
            out.write(s);

        } catch (Exception e) {
            e.printStackTrace();
            out.write(e.getMessage());
        } finally {
            osw.flush();
            out.flush();
            osw.close();
            out.close();

        }
    }

}
