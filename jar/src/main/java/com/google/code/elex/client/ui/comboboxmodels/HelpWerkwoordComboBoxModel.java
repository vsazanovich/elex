package com.google.code.elex.client.ui.comboboxmodels;

/**
 * User: Vitaly Sazanovich
 * Date: 12/19/12
 * Time: 2:55 PM
 */
public class HelpWerkwoordComboBoxModel extends AbstractComboBoxModel {

    public HelpWerkwoordComboBoxModel() {
        super();
    }

    @Override
    protected String[] getKeys() {
        return new String[]{"h", "z","hz"};
    }
}
