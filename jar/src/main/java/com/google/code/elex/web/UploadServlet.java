package com.google.code.elex.web;

import com.google.code.elex.api.Constants;
import com.google.code.elex.model.CompressedDictionary;
import com.google.code.elex.service.DictionariesPool;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.CharEncoding;
import sun.misc.BASE64Decoder;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.Iterator;
import java.util.List;

/**
 * Author: Vitaly Sazanovich
 * Email: Vitaly.Sazanovich@gmail.com
 * Date: 2/11/14
 */
public class UploadServlet extends ElexServlet {
    private static String USERPASS;
    private int maxFileSize = 100 * 1000000;
    private int maxMemSize = 4 * 1024;

    protected void doGet(HttpServletRequest request,
                         HttpServletResponse response) throws ServletException, IOException {
        PrintWriter out = null;
        OutputStreamWriter osw = null;

        try {
            String auth = request.getHeader("Authorization");
            // Do we allow that user?
            if (!allowUser(auth)) {
                // Not allowed, so report he's unauthorized
                response.setHeader("WWW-Authenticate", "BASIC realm=\"jswan test\"");
                response.sendError(response.SC_UNAUTHORIZED);
                // Could offer to add him to the allowed user list
            } else {
                String action = request.getParameter("a");
                if (action != null) {
                    if (action.equals("download")) {
                        int dicId = Integer.parseInt(request.getParameter("id"));
                        CompressedDictionary cd = DictionariesPool.getInstance().getDictionaryById(dicId);
                        response.setContentType("application/octet-stream");
                        response.setHeader("Content-Disposition", "filename=\"" + cd.getProperties().getProperty(Constants.DICTIONARY_FILE_NAME) + "\"");
                        FileInputStream fis = new FileInputStream(Constants.ELEX_PROPS.getProperty("content.folder") + File.separator + cd.getProperties().getProperty(Constants.DICTIONARY_FILE_NAME));
                        IOUtils.copy(fis, response.getOutputStream());
                        response.getOutputStream().flush();
                        response.sendRedirect("/upload");
                        return;
                    } else if (action.equals("delete")) {
                        int dicId = Integer.parseInt(request.getParameter("id"));
                        CompressedDictionary cd = DictionariesPool.getInstance().getDictionaryById(dicId);
                        File file = new File(Constants.ELEX_PROPS.getProperty("content.folder") + File.separator + cd.getProperties().getProperty(Constants.DICTIONARY_FILE_NAME));
                        file.delete();
                        DictionariesPool.DICTIONARIES.remove(dicId);
                        response.sendRedirect("/upload");
                        return;
                    }
                }

                osw = new OutputStreamWriter(response.getOutputStream(), CharEncoding.UTF_8);
                out = new PrintWriter(osw, true);

                request.setCharacterEncoding(CharEncoding.UTF_8);
                response.setCharacterEncoding(CharEncoding.UTF_8);
                response.setContentType("text/html; charset=UTF-8");

                StringBuilder sb = new StringBuilder();
                sb.append("<html><body><table border='1'><thead><td>ID</td><td>Name</td><td>Short Name</td>" +
                        "<td>Source Language</td><td>Target Language</td><td>Download</td><td>Delete</td></thead>");
                for (CompressedDictionary cd : DictionariesPool.DICTIONARIES.values()) {
                    sb.append("<tr>");
                    sb.append("<td>" + cd.getProperties().get(Constants.DICTIONARY_ID) + "</td>");
                    sb.append("<td>" + cd.getProperties().getProperty(Constants.DICTIONARY_NAME) + "</td>");
                    sb.append("<td>" + cd.getProperties().getProperty(Constants.DICTIONARY_NAME_SHORT) + "</td>");
                    sb.append("<td>" + cd.getProperties().getProperty(Constants.DICTIONARY_SOURCE_LANGUAGE) + "</td>");
                    sb.append("<td>" + cd.getProperties().getProperty(Constants.DICTIONARY_TARGET_LANGUAGE) + "</td>");
                    sb.append("<td><a href='/upload?a=download&id=" + cd.getProperties().get(Constants.DICTIONARY_ID) + "'>Download</td>");
                    sb.append("<td><a href='/upload?a=delete&id=" + cd.getProperties().get(Constants.DICTIONARY_ID) + "'>Delete</td>");
                    sb.append("</tr>");
                }
                sb.append("</table>");
                sb.append("<br/><br/><form method=\"POST\" action=\"upload\" enctype=\"multipart/form-data\" >\n" +
                        "            File:\n" +
                        "            <input type=\"file\" name=\"file\" id=\"file\" /> <br/>\n" +
                        "            </br>\n" +
                        "            <input type=\"submit\" value=\"Upload\" name=\"upload\" id=\"upload\" />\n" +
                        "        </form>");
                sb.append("</body></html>");
                out.write(sb.toString());
            }
        } catch (Exception e) {
            e.printStackTrace();
            out.write(e.getMessage());
        } finally {
            if (osw != null) osw.flush();
            if (out != null) out.flush();
            if (osw != null) osw.close();
            if (out != null) out.close();

        }
    }

    public void doPost(HttpServletRequest request,
                       HttpServletResponse response)
            throws ServletException, java.io.IOException {
        DiskFileItemFactory factory = new DiskFileItemFactory();
        // maximum size that will be stored in memory


        // Create a new file upload handler
        ServletFileUpload upload = new ServletFileUpload(factory);
        // maximum file size to be uploaded.
        upload.setSizeMax(maxFileSize);

        try {
            // Parse the request to get file items.
            List fileItems = upload.parseRequest(request);

            // Process the uploaded file items
            Iterator i = fileItems.iterator();

            while (i.hasNext()) {
                FileItem fi = (FileItem) i.next();
                if (!fi.isFormField()) {
                    // Get the uploaded file parameters
                    String fieldName = fi.getFieldName();
                    String fileName = fi.getName();
                    String contentType = fi.getContentType();
                    boolean isInMemory = fi.isInMemory();
                    long sizeInBytes = fi.getSize();
                    // Write the file
                    File file = new File(Constants.ELEX_PROPS.getProperty("content.folder") + File.separator + fileName);
                    fi.write(file);
                }
            }
        } catch (Exception ex) {
            System.out.println(ex);
        }
        response.sendRedirect("/upload");
    }

    protected boolean allowUser(String auth) throws IOException {

        if (auth == null) {
            return false;  // no auth
        }
        if (!auth.toUpperCase().startsWith("BASIC ")) {
            return false;  // we only do BASIC
        }
        // Get encoded user and password, comes after "BASIC "
        String userpassEncoded = auth.substring(6);
        // Decode it, using any base 64 decoder
        BASE64Decoder dec = new BASE64Decoder();
        String userpassDecoded = new String(dec.decodeBuffer(userpassEncoded));

        if (USERPASS == null) {
            USERPASS = userpassDecoded;
            return true;
        } else {
            return userpassDecoded.equals(USERPASS);
        }

    }

}
