package com.google.code.elex.pack;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.de.GermanAnalyzer;
import org.apache.lucene.analysis.en.EnglishAnalyzer;
import org.apache.lucene.analysis.es.SpanishAnalyzer;
import org.apache.lucene.analysis.fr.FrenchAnalyzer;
import org.apache.lucene.analysis.nl.DutchAnalyzer;
import org.apache.lucene.analysis.ru.RussianAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.IntField;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.util.Version;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;

/**
 * Author: Vitaly Sazanovich
 * Email: Vitaly.Sazanovich@gmail.com
 * Date: 11/27/13
 */
public class FullTextIndexer {

    public static void main(String[] args) throws Exception {
        if (args.length != 4) {
            throw new Exception("4 args expected");
        }
        Analyzer analyzer = null;
        if (args[3].equals("russian")) {
            analyzer = new RussianAnalyzer(Version.LUCENE_46);
        } else if (args[3].equals("dutch")) {
            analyzer = new DutchAnalyzer(Version.LUCENE_46);
        } else if (args[3].equals("english")) {
            analyzer = new EnglishAnalyzer(Version.LUCENE_46);
        } else if (args[3].equals("german")) {
            analyzer = new GermanAnalyzer(Version.LUCENE_46);
        } else if (args[3].equals("spanish")) {
            analyzer = new SpanishAnalyzer(Version.LUCENE_46);
        } else if (args[3].equals("french")) {
            analyzer = new FrenchAnalyzer(Version.LUCENE_46);
        }

        FullTextIndexer fullTextIndexer = new FullTextIndexer();
//        fullTextIndexer.createIndex("dictionaries/ushakov/", "dictionaries/ushakov/Ushakov.dsl", "dictionaries/ushakov/Ushakov.idx", new RussianAnalyzer(Version.LUCENE_46));
//        fullTextIndexer.createIndex("dictionaries/webster/", "dictionaries/webster/Webster.dsl", "dictionaries/webster/Webster.idx", new EnglishAnalyzer(Version.LUCENE_46));
        fullTextIndexer.createIndex(args[0], args[1], args[2], analyzer);
    }

    public void createIndex(String where, String sourceFileLocation, String indexFileLocation, Analyzer analyzer) throws Exception {

//        Analyzer analyzer = new RussianAnalyzer(Version.LUCENE_46);
        Directory directory = FSDirectory.open(new File(where));

        IndexWriterConfig config = new IndexWriterConfig(Version.LUCENE_46, analyzer);

        IndexWriter w = new IndexWriter(directory, config);

        int counter = 1;
        EntryRetriever entryRetriever = new EntryRetriever();
        String entry = entryRetriever.retrieveEntry(new RandomAccessFile(sourceFileLocation, "r"), new RandomAccessFile(indexFileLocation, "r"), counter);
        while (entry != null) {
            try {
//                System.out.print(counter + ",");
                if (counter % 1000 == 0) {
                    System.out.println(counter);
                }
                entry = stripTags(entry);
                String hw = entry.split("\\r|\\n|\\t")[0];
                addDoc(w, counter, hw, entry);
                ++counter;
                entry = entryRetriever.retrieveEntry(new RandomAccessFile(sourceFileLocation, "r"), new RandomAccessFile(indexFileLocation, "r"), counter);
            } catch (Exception ex) {
                ex.printStackTrace();
                throw new RuntimeException(ex.getMessage());
            }
        }
        w.commit();
        w.close();
    }

    private String stripTags(String entry) {
        entry = entry.replaceAll("\\[[^]]+\\]", "");
        entry = entry.replaceAll("\t|\r|\n", "");
        return entry;
    }

    private static void addDoc(IndexWriter w, int offset, String hw, String entry) throws IOException {
        Document doc = new Document();
        doc.add(new IntField("offset", offset, Field.Store.YES));
        doc.add(new TextField("headword", hw, Field.Store.NO));
        doc.add(new TextField("content", entry, Field.Store.NO));
        w.addDocument(doc);
    }
}
