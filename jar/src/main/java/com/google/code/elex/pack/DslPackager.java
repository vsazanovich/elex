package com.google.code.elex.pack;

import com.google.code.elex.model.CompressedDictionary;
import com.google.code.elex.model.DictionarySectionDescriptor;
import com.google.code.elex.service.ElexUtils;

import java.io.*;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

/**
 * Author: Vitaly Sazanovich
 * Email: vitaly.sazanovich@gmail.com
 * Date: 27/11/13
 */
public class DslPackager {

    public static void main(String[] args) throws Exception {
        if (args.length != 3) {
            throw new Exception("3 args expected");
        }
        DslPackager dslPackager = new DslPackager();
        try {
            if (args[0].equals("package")) {
                dslPackager.packageDictionary(args[1], args[2]);
            } else if (args[0].equals("unpackage")) {
                dslPackager.unpackageDictionary(args[1], args[2]);
            } else if (args[0].equals("cache")) {
                dslPackager.unpackageForCache(args[1], args[2]);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * @param path           where the files to be packaged are located
     * @param outputFileName all files should have the same prefix
     * @throws Exception
     */
    public static void packageDictionary(String path, String outputFileName) throws Exception {
        BufferedOutputStream os = new BufferedOutputStream(new GZIPOutputStream(new FileOutputStream(outputFileName)));
        File[] files = new File(path).listFiles();
        int offset = 0;
        SortedSet<DictionarySectionDescriptor> descriptors = new TreeSet<DictionarySectionDescriptor>();
        for (File file : files) {
            DictionarySectionDescriptor descriptor = new DictionarySectionDescriptor();
            descriptor.size = (int) file.length();
            descriptor.offset = offset;
            descriptor.name = file.getName();
            offset += descriptor.size;
            descriptors.add(descriptor);
            System.out.println(file.getName());

            BufferedInputStream fis = new BufferedInputStream(new FileInputStream(file));

            int count = descriptor.size;
            while (count > 0) {
                os.write(fis.read());
                --count;
            }
            fis.close();
        }

        DictionarySectionDescriptor[] descriptorsArr = descriptors.toArray(new DictionarySectionDescriptor[descriptors.size()]);
        byte[] headerBytes = ElexUtils.serialize(descriptorsArr);
        int headerSize = headerBytes.length;

        os.write(headerBytes);
        os.write(ElexUtils.int2bytes(headerSize));

        os.close();
    }

    public static void unpackageForCache(String pathToDictionary, String pathToCacheFile) throws Exception {
        FileInputStream fis = null;
        GZIPInputStream gzip = null;
        BufferedInputStream bis = null;

        FileOutputStream fos = null;
        BufferedOutputStream bos = null;
        try {
            fis = new FileInputStream(pathToDictionary);
            gzip = new GZIPInputStream(fis);
            bis = new BufferedInputStream(gzip);

            fos = new FileOutputStream(pathToCacheFile);
            bos = new BufferedOutputStream(fos);

            int sChunk = 8192;
            byte[] buffer = new byte[sChunk];
            int length;
            while ((length = bis.read(buffer, 0, sChunk)) != -1) {
                bos.write(buffer, 0, length);
            }

            bos.flush();
            fos.flush();
        } finally {
            if (bos != null) bos.close();
            if (fos != null) fos.close();


            if (bis != null) bis.close();
            if (gzip != null) gzip.close();
            if (fis != null) fis.close();
        }

    }


    public static void unpackageDictionary(String pathToDictionary, String outputFolder) throws Exception {
        if (!outputFolder.endsWith("/")) {
            outputFolder += "/";
        }
        File out = new File(outputFolder);
        out.mkdirs();
        CompressedDictionary cd = new CompressedDictionary("tmp/", pathToDictionary);
        SortedSet<DictionarySectionDescriptor> ss = cd.getDictionarySectionDescriptors();
        for (DictionarySectionDescriptor dd : ss) {
            System.out.println(dd.name);
            FileOutputStream fos = new FileOutputStream(outputFolder + dd.name);
            cd.writeSectionBytes(fos, dd);
        }
    }


}
