package com.google.code.elex.model;

import com.google.code.elex.api.IPeekIterator;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

/**
 * Author: Vitaly Sazanovich
 * Email: vitaly.sazanovich@gmail.com
 * Date: 05/02/14
 */
public class IndexIterator implements IPeekIterator {
    //points at next
    int ind;
    boolean isForward;
    CompressedDictionary compressedDictionary;

    public IndexIterator(CompressedDictionary cd, String from, boolean iF) throws Exception {
        isForward = iF;
        compressedDictionary = cd;
        ind = cd.findInsertionPoint(from);
        if (ind < 0) {
            ind = Math.abs(ind)-1;
            if (!isForward) {
                ind -= 1;
            }
        }
    }


    @Override
    public boolean hasNext() {
        if (ind < 0 || ind >= compressedDictionary.getSize()) {
            return false;
        }
        return true;
    }


    @Override
    public String next() {
        try {
            if (!hasNext()) {
                return null;
            }
            return compressedDictionary.entryRetriever.retrieveHeadword(isForward ? ind++ : ind--);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public String peek() {
        try {
            if (!hasNext()) {
                return null;
            }
            return compressedDictionary.entryRetriever.retrieveHeadword(isForward ? ind : ind);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public void remove() {
        throw new NotImplementedException();
    }
}
