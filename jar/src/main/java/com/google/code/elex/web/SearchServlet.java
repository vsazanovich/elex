package com.google.code.elex.web;

import com.google.code.elex.model.CompressedDictionary;
import com.google.code.elex.model.SearchResult;
import com.google.code.elex.service.DictionariesPool;
import com.google.gson.Gson;
import org.apache.commons.lang3.CharEncoding;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.util.Set;

/**
 * Author: Vitaly Sazanovich
 * Email: vitaly.sazanovich@gmail.com
 * Date: 17/02/14
 * Time: 16:42
 */
public class SearchServlet extends ElexServlet {

    protected void doGet(HttpServletRequest request,
                         HttpServletResponse response) throws ServletException, IOException {
        OutputStreamWriter osw = new OutputStreamWriter(response.getOutputStream(), CharEncoding.UTF_8);
        PrintWriter out = new PrintWriter(osw, true);
        try {
            request.setCharacterEncoding(CharEncoding.UTF_8);
            response.setCharacterEncoding(CharEncoding.UTF_8);
            response.setContentType("application/json; charset=UTF-8");

            Set<CompressedDictionary> set = getDics(request);
            String searchValue = request.getParameter("sv");
            SearchResult sr = DictionariesPool.getInstance().search(set, searchValue);


            Gson gson = new Gson();
            String s = gson.toJson(sr.getResults());
            out.write(s);


        } catch (Exception e) {
            e.printStackTrace();
            out.write(e.getMessage());
        } finally {
            osw.flush();
            out.flush();
            osw.close();
            out.close();
        }
    }

}
