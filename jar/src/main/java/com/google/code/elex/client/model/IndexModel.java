package com.google.code.elex.client.model;

import java.io.Serializable;
import java.util.List;

/**
 * User: Vitaly Sazanovich
 * Date: 29/10/12
 * Time: 14:38
 * Email: Vitaly.Sazanovich@gmail.com
 */
public class IndexModel implements Serializable {
    private int pageSize;
    private int indexLength;
    private List<DictionaryEntry> visibleHeadwords;

    public IndexModel() {

    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public int getIndexLength() {
        return indexLength;
    }

    public void setIndexLength(int indexLength) {
        this.indexLength = indexLength;
    }

    public List<DictionaryEntry> getVisibleHeadwords() {
        return visibleHeadwords;
    }

    public void setVisibleHeadwords(List<DictionaryEntry> visibleHeadwords) {
        this.visibleHeadwords = visibleHeadwords;
    }
}
