package com.google.code.elex.client.ui.comboboxmodels;

import com.google.code.elex.client.MyThreadLocal;
import com.google.code.elex.client.Utf8ResourceBundle;

import java.util.Locale;

/**
 * User: Vitaly Sazanovich
 * Date: 12/18/12
 * Time: 9:45 PM
 */
public class PosComboBoxModel extends AbstractComboBoxModel {

    public PosComboBoxModel() {
        super();
    }

    @Override
    protected String[] getKeys() {
        String lang = MyThreadLocal.get().getLang();
        String pos = Utf8ResourceBundle.getBundle("DictiographyResource", new Locale(lang), this.getClass().getClassLoader()).getString("pos");
        return pos.split(",");
    }
}
