package com.google.code.elex.client;

/**
 * User: Vitaly Sazanovich
 * Date: 05/12/12
 * Time: 19:43
 * Email: Vitaly.Sazanovich@gmail.com
 */


//import chrriis.common.UIUtils;
//import chrriis.dj.nativeswing.swtimpl.NativeInterface;
//import chrriis.dj.nativeswing.swtimpl.components.JWebBrowser;
//import chrriis.dj.nativeswing.swtimpl.components.WebBrowserAdapter;
//import chrriis.dj.nativeswing.swtimpl.components.WebBrowserCommandEvent;
import com.google.code.elex.client.entry.EntryObjectModel;
import com.google.code.elex.client.ui.Bindable;
import com.google.code.elex.client.ui.dialogs.EntryDialog;
import com.google.code.elex.shared.BCrypt;
import com.google.code.elex.shared.ElexConstants;
import com.google.code.elex.shared.ElexUtils;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.*;
import java.net.*;
import java.util.ArrayList;


public class AppMain extends JPanel implements Bindable {
    private static JFrame frame;
//    public static JWebBrowser webBrowser;

    JButton addBtn;
    JButton editBtn;
    JButton deleteBtn;
    JButton refreshBtn;
    JButton licenseBtn;
    JButton settingBtn;
    JButton helpBtn;
    JLabel licMessage = new JLabel(" This is a trial version. Visit our site dictiography.com to buy a license.");

    EntryDialog entryDialog;
    private static String baseUrl = "http://localhost:8080";

    public AppMain() {
        super(new BorderLayout());
        JPanel webBrowserPanel = new JPanel(new BorderLayout());
//        webBrowser = new JWebBrowser();
//
//        webBrowser.setStatusBarVisible(false);
//        webBrowser.setMenuBarVisible(false);
//        webBrowser.setLocationBarVisible(false);
//        webBrowser.setButtonBarVisible(false);
//
//        webBrowser.add(getToolbar(), BorderLayout.SOUTH);
//        webBrowser.addWebBrowserListener(new WebBrowserAdapter() {
//            @Override
//            public void commandReceived(WebBrowserCommandEvent e) {
//                String command = e.getCommand();
//                Object[] parameters = e.getParameters();
//                if ("forward".equals(command)) {
//                }
//            }
//        });
//
//        webBrowserPanel.add(webBrowser, BorderLayout.CENTER);
        add(webBrowserPanel, BorderLayout.CENTER);

    }

    /* Standard main method to try that test as a standalone application. */
    public static void main(String[] args) {
//        com.sun.media.codec.audio.mp3.JavaDecoder.main(null);
//        UIUtils.setPreferredLookAndFeel();
//        NativeInterface.open();
//        SwingUtilities.invokeLater(new Runnable() {
//            public void run() {
//                frame = new JFrame("Dictiography");//+ " v."+ElexConstants.VERSION);
//                frame.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
//                frame.addWindowListener(new WindowAdapter() {
//                    public void windowClosing(WindowEvent ev) {
//                        stopServer();
//                        frame.dispose();
//                    }
//                });
//                AppMain appMain = new AppMain();
//                frame.getContentPane().add(appMain, BorderLayout.CENTER);
//
//                frame.setSize(900, 700);
//                frame.setIconImage(new ImageIcon(getClass().getClassLoader().getResource("com/dictiography/client/assets/favicon.png")).getImage());
//                frame.setLocationByPlatform(true);
//                webBrowser.navigate(baseUrl + "/index.html?rnd=" + System.currentTimeMillis());
////                webBrowser.navigate("http://www.google.com");
//                frame.setVisible(true);
//            }
//        });
//        NativeInterface.runEventPump();
    }

    private static void stopServer() {
        try {
            Socket echoSocket = new Socket(ElexUtils.getWinstoneProperties().getProperty("httpListenAddress"), Integer.parseInt(ElexUtils.getWinstoneProperties().getProperty("controlPort")));
            PrintWriter out = new PrintWriter(echoSocket.getOutputStream(), true);
            out.write("0");
            out.close();
            echoSocket.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

//    public static void main(String[] args) {
//        EntryDialog ed = new EntryDialog(frame, "Add entry", true);
//        ed.pack();
//    }

    private JToolBar getToolbar() {
        JToolBar toolbar = new JToolBar("Toolbar", JToolBar.HORIZONTAL);
        toolbar.setFloatable(false);

        addBtn = new JButton(new ImageIcon(getClass().getClassLoader().getResource("com/dictiography/client/assets/application_form_add.png")));
        addBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ThreadContext context = new ThreadContext();
                context.setLang(getLang());
                MyThreadLocal.set(context);
                entryDialog = new EntryDialog(AppMain.this, null, ElexConstants.NEW_ACTION);
                entryDialog.container.setVisible(true);
            }
        });
        toolbar.add(addBtn);

        editBtn = new JButton(new ImageIcon(getClass().getClassLoader().getResource("com/dictiography/client/assets/application_form_edit.png")));
        editBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    EntryObjectModel eom = retrieveData();
                    if (eom == null) {
                        return;
                    }
                    ThreadContext context = new ThreadContext();
                    context.setLang(getLang());
                    MyThreadLocal.set(context);
                    entryDialog = new EntryDialog(AppMain.this, eom, ElexConstants.UPDATE_ACTION);
                    entryDialog.container.setVisible(true);
                } catch (Exception e1) {
                    e1.printStackTrace();
                }
            }
        });
        editBtn.setEnabled(true);
        toolbar.add(editBtn);

        deleteBtn = new JButton(new ImageIcon(getClass().getClassLoader().getResource("com/dictiography/client/assets/application_form_delete.png")));
        deleteBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    String res = getSelectedHeadword();
                    if (res != null && !res.equals("")) {

                        int response = showConfirmDialog(null,
                                ElexUtils.getProperty(getLang(),
                                        "messsage.remove") + " '" + res + "'?",
                                ElexUtils.getProperty(getLang(), "title.confirm"),
                                JOptionPane.YES_NO_CANCEL_OPTION);

                        if (response == JOptionPane.YES_OPTION) {
                            removeEntry();
//                            AppMain.webBrowser.executeJavascript("reloadIndex()");
                        }
                    }
                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(entryDialog.container, ex.getMessage());
                }
            }
        });
        deleteBtn.setEnabled(true);
        toolbar.add(deleteBtn);

        refreshBtn = new JButton(new ImageIcon(getClass().getClassLoader().getResource("com/dictiography/client/assets/arrow_refresh.png")));
        refreshBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
//                    webBrowser.navigate(baseUrl + "/index.html?rnd=" + System.currentTimeMillis());
                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(entryDialog.container, ex.getMessage());
                }
            }
        });
        refreshBtn.setEnabled(true);
        toolbar.add(refreshBtn);

        licenseBtn = new JButton(new ImageIcon(getClass().getClassLoader().getResource("com/dictiography/client/assets/license_key.png")));
        licenseBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    String input = JOptionPane.showInputDialog(frame, "License key:");
                    if (input==null){
                        return;
                    }
                    long size = new File("dictiography.jar").length();
                    boolean res = BCrypt.checkpw(String.valueOf(size), input);
                    if (!res) {
                        throw new Exception("License is incorrect");
                    }
                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(frame, "License  is incorrect");
                }
            }
        });
        licenseBtn.setEnabled(true);
        toolbar.add(licenseBtn);

        try {
            FileInputStream fis = new FileInputStream("lic.txt");
            byte[] bbs = new byte[fis.available()];
            fis.read(bbs);
            fis.close();
            ;
            long size = new File("dictiography.jar").length();
            boolean res = BCrypt.checkpw(String.valueOf(size), new String(bbs));
            if (!res) {
                throw new Exception("License is incorrect");
            } else {
                licMessage.getParent().remove(licMessage);
            }
        } catch (Exception ex) {
            licMessage.setForeground(Color.RED);
            toolbar.add(licMessage);
        }

//        settingBtn = new JButton(new ImageIcon(getClass().getClassLoader().getResource("com/dictiography/client/assets/setting_tools.png")));
//        settingBtn.addActionListener(new ActionListener() {
//            @Override
//            public void actionPerformed(ActionEvent e) {
//                try {
////                    webBrowser.reloadPage();
//                } catch (Exception ex) {
//                    JOptionPane.showMessageDialog(entryDialog.container, ex.getMessage());
//                }
//            }
//        });
//        settingBtn.setEnabled(true);
//        toolbar.add(settingBtn);
//
//        helpBtn = new JButton(new ImageIcon(getClass().getClassLoader().getResource("com/dictiography/client/assets/help.png")));
//        helpBtn.addActionListener(new ActionListener() {
//            @Override
//            public void actionPerformed(ActionEvent e) {
//                try {
////                    webBrowser.reloadPage();
//                } catch (Exception ex) {
//                    JOptionPane.showMessageDialog(entryDialog.container, ex.getMessage());
//                }
//            }
//        });
//        helpBtn.setEnabled(true);
//        toolbar.add(helpBtn);


        return toolbar;
    }

    public static int showConfirmDialog(Component parentComponent, Object message, String title, int optionType) {
        ArrayList<Object> options = new ArrayList<Object>();
        Object defaultOption;
        switch (optionType) {
            case JOptionPane.OK_CANCEL_OPTION:
                options.add(UIManager.getString("OptionPane.okButtonText"));
                options.add(UIManager.getString("OptionPane.cancelButtonText"));
                defaultOption = UIManager.getString("OptionPane.cancelButtonText");
                break;
            case JOptionPane.YES_NO_OPTION:
                options.add(UIManager.getString("OptionPane.yesButtonText"));
                options.add(UIManager.getString("OptionPane.noButtonText"));
                defaultOption = UIManager.getString("OptionPane.noButtonText");
                break;
            case JOptionPane.YES_NO_CANCEL_OPTION:
                options.add(UIManager.getString("OptionPane.yesButtonText"));
                options.add(UIManager.getString("OptionPane.noButtonText"));
                options.add(UIManager.getString("OptionPane.cancelButtonText"));
                defaultOption = UIManager.getString("OptionPane.cancelButtonText");
                break;
            default:
                throw new IllegalArgumentException("Unknown optionType " + optionType);
        }
        return JOptionPane.showOptionDialog(parentComponent, message, title, optionType, JOptionPane.QUESTION_MESSAGE, null, options.toArray(), defaultOption);
    }


    public static EntryObjectModel retrieveData() throws Exception {
        if (getSelectedEntryId() == null || getSelectedEntryId().equals("")) return null;
        URL url = new URL(baseUrl + "/entry?rnd=" + System.currentTimeMillis() + "&" +
                ElexConstants.ENTRY_ID_PARAM_NAME + "=" + getSelectedEntryId() + "&" +
                ElexConstants.LOCALE_PARAM_NAME + "=" + getLang() + "&" +
                ElexConstants.IS_SOURCE_PARAM_NAME + "=true");

        URLConnection con = url.openConnection();
        con.setConnectTimeout(10000);
        con.setReadTimeout(10000);
        InputStream in = con.getInputStream();
        BufferedReader dis = new BufferedReader(new InputStreamReader(in));
        String s;
        StringBuffer sb = new StringBuffer();
        while ((s = dis.readLine()) != null) {
            sb.append(s);
            sb.append("\n");
        }
        in.close();
        return (EntryObjectModel) ElexUtils.xml2entry(sb.toString());
    }


    public void postData(EntryObjectModel eom) throws Exception {
//        System.out.println(ElexUtils.entry2xml(eom));
//        if (eom!=null)return;
        // Construct data
        String data = ElexConstants.HEADWORD_PARAM_NAME + "=" + URLEncoder.encode(eom.getHeadword(), "UTF-8") + "&" +
                ElexConstants.DATA_PARAM_NAME + "=" + URLEncoder.encode(ElexUtils.entry2xml(eom), "UTF-8");

        // Send data
        String selE = entryDialog.mode.equals(ElexConstants.NEW_ACTION)?"":ElexConstants.ENTRY_ID_PARAM_NAME + "=" + getSelectedEntryId() + "&";
        String action = ElexConstants.ACTION_PARAM_NAME + "=" + entryDialog.mode + "&";
        String locale = ElexConstants.LOCALE_PARAM_NAME + "=" + getLang() + "&";
        String rnd = ElexConstants.RND_PARAM_NAME + "=" + System.currentTimeMillis();

        URL url = new URL(baseUrl + "/entry?" + locale + selE + action + rnd);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
        conn.setRequestMethod("POST");
        conn.setDoOutput(true);
        OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
        wr.write(data);
        wr.flush();

        // Get the response
        BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
        String line;
        while ((line = rd.readLine()) != null) {
            if (!line.equals("")) {
                wr.close();
                rd.close();
                throw new Exception(line);
            }
        }
        wr.close();
        rd.close();
    }


    public static void removeEntry() throws Exception {
        // Send data
        URL url = new URL(baseUrl + "/entry?"
                + ElexConstants.LOCALE_PARAM_NAME + "=" + getLang() + "&"
                + ElexConstants.REMOVE_PARAM_NAME + "=true&"
                + ElexConstants.ENTRY_ID_PARAM_NAME + "=" + getSelectedEntryId() + "&"
                + ElexConstants.RND_PARAM_NAME + "=" + System.currentTimeMillis());

        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
        conn.setRequestMethod("POST");
        conn.setDoOutput(true);
        OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
        wr.flush();

        // Get the response
        BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
        String line;
        while ((line = rd.readLine()) != null) {
            if (!line.equals("")) {
                wr.close();
                rd.close();
                throw new Exception(line);
            }
        }
        wr.close();
        rd.close();
    }

    // Center on screen ( absolute true/false (exact center or 25% upper left) )
    public static void centerOnScreen(final Component c, final boolean absolute) {
        final int width = c.getWidth();
        final int height = c.getHeight();
        final Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        int x = (screenSize.width / 2) - (width / 2);
        int y = (screenSize.height / 2) - (height / 2);
        if (!absolute) {
            x /= 2;
            y /= 2;
        }
        c.setLocation(x, y);
    }

    // Center on parent ( absolute true/false (exact center or 25% upper left) )
    public static void centerOnParent(final Window child, final boolean absolute) {
        child.pack();
        boolean useChildsOwner = child.getOwner() != null ? ((child.getOwner() instanceof JFrame) || (child.getOwner() instanceof JDialog)) : false;
        final Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        final Dimension parentSize = useChildsOwner ? child.getOwner().getSize() : screenSize;
        final Point parentLocationOnScreen = useChildsOwner ? child.getOwner().getLocationOnScreen() : new Point(0, 0);
        final Dimension childSize = child.getSize();
        childSize.width = Math.min(childSize.width, screenSize.width);
        childSize.height = Math.min(childSize.height, screenSize.height);
        child.setSize(childSize);
        int x;
        int y;
        if ((child.getOwner() != null) && child.getOwner().isShowing()) {
            x = (parentSize.width - childSize.width) / 2;
            y = (parentSize.height - childSize.height) / 2;
            x += parentLocationOnScreen.x;
            y += parentLocationOnScreen.y;
        } else {
            x = (screenSize.width - childSize.width) / 2;
            y = (screenSize.height - childSize.height) / 2;
        }
        if (!absolute) {
            x /= 2;
            y /= 2;
        }
        child.setLocation(x, y);
    }

    @Override
    public void setData(Object d) {
        EntryObjectModel eom = (EntryObjectModel) d;
        try {
            postData(eom);
            ((JDialog) entryDialog.container).dispose();
//            AppMain.webBrowser.executeJavascript("loadIndexBySearch('" + URLEncoder.encode(eom.getHeadword(), "UTF-8") + "')");
        } catch (Exception e) {
            e.printStackTrace();
            JOptionPane.showMessageDialog(entryDialog.container, ElexUtils.getProperty(getLang(), e.getMessage()));
        }

    }

    @Override
    public Object getData(Object data) {
        return null;
    }

    @Override
    public boolean isEmpty() {
        return false;
    }

    @Override
    public JPanel getMainPanel() {
        return null;
    }


    public static String getLang() {
        return null;//webBrowser.executeJavascriptWithResult("return getLang()").toString();
    }

    public static String getSelectedHeadword() {
        return null;//webBrowser.executeJavascriptWithResult("return getSelectedHeadword()").toString();
    }

    public static String getSelectedEntryId() {
//        Object res = webBrowser.executeJavascriptWithResult("return getSelectedEntryId(false)");
//        return res == null ? "" : String.valueOf(((Double) res).intValue());
        return null;
    }
}