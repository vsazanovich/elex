package com.google.code.elex.pack;

import com.google.code.elex.service.ElexUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.CharEncoding;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.SortedMap;
import java.util.TreeMap;

/**
 * Author: Vitaly Sazanovich
 * Email: vitaly.sazanovich@gmail.com
 * Date: 18/02/14
 */
public class ElexResourcesPackager {
    static int counter = 0;

    public static void main(String[] args) throws Exception {
        if (args.length != 3) {
            throw new Exception("3 args expected");
        }
        if (args[0].equals("package")) {
            packageResources(args[1], args[2]);
        }
    }


    public static void packageResources(String path, String outputFile) throws Exception {
        if (!path.endsWith("/")) {
            path += "/";
        }
        SortedMap<String, Long> sizes = new TreeMap<String, Long>();
        String basePath = path;
        collectSizes(basePath, sizes, new File(path));
        BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(outputFile));
        int maxNameLength = 0;
        counter = 0;
        //writing data block
        for (String s : sizes.keySet()) {
            long size = sizes.get(s);
            if (s.length() > maxNameLength) {
                maxNameLength = s.length();
            }
//            System.out.println(s + "->" + ElexUtils.getMD5Checksum(new FileInputStream(path + s)));
            IOUtils.copyLarge(new FileInputStream(path + s), bos, 0, size);
            ++counter;
            if (counter % 10000 == 0) {
                System.out.println("Saving to output: " + counter);
            }
        }
        if (sizes.isEmpty()) {
            return;
        }
        //writing file names block
        int fileNamesSize = 0;
        for (String s : sizes.keySet()) {
            byte[] bbs = (s + '\n').getBytes(CharEncoding.UTF_8);
            bos.write(bbs);
            fileNamesSize += bbs.length;
        }

        //writing file names offsets block
        int fileNamesOffset = 0;
        for (String s : sizes.keySet()) {
            byte[] bbs = (s + '\n').getBytes(CharEncoding.UTF_8);
            fileNamesOffset += bbs.length;
            bos.write(ElexUtils.int2bytes(fileNamesOffset));
        }
        //writing offsets block
        long offset = 0;
        for (String s : sizes.keySet()) {
            long size = sizes.get(s);
            long newOffset = offset + size;
            bos.write(ElexUtils.longToBytes(newOffset));
            offset += size;
        }
        bos.write(ElexUtils.int2bytes(fileNamesSize));
        bos.write(ElexUtils.int2bytes(sizes.size()));
        bos.close();
    }


    private static void collectSizes(String basePath, SortedMap<String, Long> offsets, File f) {
        File[] ffs = f.listFiles();
        for (File file : ffs) {
            if (file.isDirectory()) {
                collectSizes(basePath, offsets, file);
            } else {
                String relative = new File(basePath).toURI().relativize(file.toURI()).getPath();
                offsets.put(relative, file.length());
                ++counter;
                if (counter % 10000 == 0) {
                    System.out.println("Collecting info: " + counter);
                }
            }
        }
    }


    public static void unPackageResources() {

    }


}
