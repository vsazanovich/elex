package com.google.code.elex.model;

import com.google.code.elex.service.ElexUtils;
import org.apache.commons.codec.CharEncoding;

import java.io.OutputStream;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.List;

/**
 * Author: Vitaly Sazanovich
 * Email: vitaly.sazanovich@gmail.com
 * Date: 18/02/14
 * <p/>
 * Resource package format:
 * - data block
 * - file names block
 * - file names offsets block
 * - data offsets block
 * <p/>
 * - file names block size
 * - files count
 */
public class ResourcesPackage {
    private String absolutePath;
    private int recordsCount;
    private int fileNamesSize;

    public ResourcesPackage(String n) throws Exception {
        this.absolutePath = n;

        RandomAccessFile raf = new RandomAccessFile(absolutePath, "r");
        try {
            byte[] bbs;

            raf.seek(raf.length() - 8);
            bbs = new byte[4];
            raf.read(bbs);
            fileNamesSize = ElexUtils.bytes2int(bbs);

            bbs = new byte[4];
            raf.read(bbs);
            recordsCount = ElexUtils.bytes2int(bbs);
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            raf.close();
        }
    }

    public String getAbsolutePath() {
        return absolutePath;
    }

    public void getResourceByName(OutputStream os, String resId) throws Exception {
        RandomAccessFile raf = new RandomAccessFile(absolutePath, "r");
        int count = findCountByName(raf, resId);
        if (count < 0) {
            return;
        }

        long offset = 0;
        byte[] bbs = new byte[8];
        raf.seek(raf.length() - 2 * 4 - 8 * recordsCount);
        if (count > 0) {
            raf.seek(raf.length() - 2 * 4 - 8 * recordsCount + (count - 1) * 8);
            raf.read(bbs);
            offset = ElexUtils.bytesToLong(bbs);
        }

        long nextOffset = raf.length() - 2 * 4 - 8 * recordsCount - 4 * recordsCount - fileNamesSize;
        if (count < recordsCount) {
            raf.read(bbs);
            nextOffset = ElexUtils.bytesToLong(bbs);
        }

        raf.seek(offset);
        int read = (int) (nextOffset - offset);
        for (int i = 0; i < read; i++) {
            os.write(raf.read());
        }

    }

    protected int findCountByName(RandomAccessFile raf, String key) throws Exception {
        int low = 0;
        int high = recordsCount - 1;

        while (low <= high) {
            int mid = (low + high) >>> 1;
            String midVal = getFileNameAt(raf, mid);

            if (midVal.compareTo(key) < 0)
                low = mid + 1;
            else if (midVal.compareTo(key) > 0)
                high = mid - 1;
            else
                return mid; // key found
        }
        return -(low + 1);  // key not found.
    }

    private String getFileNameAt(RandomAccessFile raf, int at) throws Exception {
        int offset = 0;
        byte[] bbs = new byte[4];
        raf.seek(raf.length() - 2 * 4 - 8 * recordsCount - 4 * recordsCount);

        if (at > 0) {
            raf.seek(raf.length() - 2 * 4 - 8 * recordsCount - 4 * recordsCount + (at - 1) * 4);
            raf.read(bbs);
            offset = ElexUtils.bytes2int(bbs);
        }

        int nextOffset = fileNamesSize;
        if (at < recordsCount) {
            raf.read(bbs);
            nextOffset = ElexUtils.bytes2int(bbs);
        }


        raf.seek(raf.length() - 2 * 4 - 8 * recordsCount - 4 * recordsCount - fileNamesSize + offset);
        bbs = new byte[nextOffset - offset];
        raf.read(bbs);
        return new String(bbs, CharEncoding.UTF_8).replaceAll("\n", "");
    }


    private class ResourceRecord {
        public String name;
        public long offset;
        public int length;
    }

    @Override
    public boolean equals(Object o) {
        if (o == null || !(o instanceof ResourcesPackage)) {
            return false;
        }
        return absolutePath.equals(((ResourcesPackage) o).getAbsolutePath());
    }

    /**
     * DEBUG
     *
     * @return
     * @throws Exception
     */


    protected List<String> getFileNames() throws Exception {
        RandomAccessFile raf = new RandomAccessFile(absolutePath, "r");
        List<String> res = new ArrayList<String>();
        for (int i = 0; i < recordsCount; i++) {
            String fn = getFileNameAt(raf, i);
            res.add(fn);
        }
        return res;
    }

    public String debug() throws Exception {
        StringBuilder sb = new StringBuilder();
        RandomAccessFile raf = new RandomAccessFile(absolutePath, "r");
        byte[] bbs = null;
        try {


            raf.seek(raf.length() - 2 * 4 - 8 * recordsCount);
            sb.append("Offsets: \n");
            for (int i = 0; i < recordsCount; i++) {
                bbs = new byte[8];
                raf.read(bbs);
                long offset = ElexUtils.bytesToLong(bbs);
                sb.append(i + ":" + offset + '\n');
            }

            raf.seek(raf.length() - 2 * 4 - 8 * recordsCount - 4 * recordsCount - fileNamesSize);
            sb.append("\nFile names: \n");
            bbs = new byte[fileNamesSize];
            raf.read(bbs);
            sb.append(new String(bbs, CharEncoding.UTF_8));

            sb.append("\nFile names using getFileNameAt: \n");
            for (int i = 0; i < recordsCount; i++) {
                String fn = getFileNameAt(raf, i) + '\n';
                sb.append(fn);
            }


        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            raf.close();
        }
        return sb.toString();
    }

}
