package com.google.code.elex.web;

import com.google.code.elex.api.Constants;
import com.google.code.elex.model.CompressedDictionary;
import com.google.code.elex.model.IndexView;
import com.google.code.elex.service.DictionariesPool;
import com.google.gson.Gson;
import org.apache.commons.lang3.CharEncoding;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.text.Collator;
import java.util.Iterator;
import java.util.Locale;
import java.util.Set;

/**
 * Author: Vitaly Sazanovich
 * Email: vitaly.sazanovich@gmail.com
 * Date: 10/12/13
 */
public class IndexServlet extends ElexServlet {

    protected void doGet(HttpServletRequest request,
                         HttpServletResponse response) throws ServletException, IOException {
        OutputStreamWriter osw = new OutputStreamWriter(response.getOutputStream(), CharEncoding.UTF_8);
        PrintWriter out = new PrintWriter(osw, true);
        try {
            request.setCharacterEncoding(CharEncoding.UTF_8);
            response.setCharacterEncoding(CharEncoding.UTF_8);
            response.setContentType("application/json; charset=UTF-8");


            int pageSize = Integer.parseInt(request.getParameter("ps"));
            int viewOffset = Integer.parseInt(request.getParameter("vo"));
            String searchValue = request.getParameter("sv");
            String action = request.getParameter("a");

            IndexView indexView = new IndexView();
            Set<CompressedDictionary> set = getDics(request);
            Collator collator = getCollactor(set);

            if (!set.isEmpty() && pageSize > 0) {
                if (action.equals(Constants.TO_END_NAVIGATION_ACTION)) {
                    int percent = 100;
                    indexView = DictionariesPool.getInstance().getIndexView(set, pageSize, viewOffset, percent, collator);
                } else if (action.equals(Constants.TO_START_NAVIGATION_ACTION)) {
                    int percent = 0;
                    indexView = DictionariesPool.getInstance().getIndexView(set, pageSize, viewOffset, percent, collator);
                } else if (action.equals(Constants.FAST_FORWARD_NAVIGATION_ACTION)) {
                    int percent = DictionariesPool.getInstance().getPercentByHeadword(set, searchValue);
                    percent += 1;
                    percent = percent > 100 ? 100 : percent;
                    percent = percent < 0 ? 0 : percent;
                    indexView = DictionariesPool.getInstance().getIndexView(set, pageSize, viewOffset, percent, collator);
                } else if (action.equals(Constants.FAST_BACKWARD_NAVIGATION_ACTION)) {
                    int percent = DictionariesPool.getInstance().getPercentByHeadword(set, searchValue);
                    percent -= 1;
                    percent = percent > 100 ? 100 : percent;
                    percent = percent < 0 ? 0 : percent;
                    indexView = DictionariesPool.getInstance().getIndexView(set, pageSize, viewOffset, percent, collator);
                } else if (action.equals(Constants.PAGE_FORWARD_NAVIGATION_ACTION)) {
                    searchValue = DictionariesPool.getInstance().scrollTo(pageSize, set, searchValue, collator);
                    indexView = DictionariesPool.getInstance().getIndexView(set, pageSize, viewOffset, searchValue, collator);
                } else if (action.equals(Constants.PAGE_BACKWARD_NAVIGATION_ACTION)) {
                    searchValue = DictionariesPool.getInstance().scrollTo(-(pageSize), set, searchValue, collator);
                    indexView = DictionariesPool.getInstance().getIndexView(set, pageSize, viewOffset, searchValue, collator);
                } else if (action.equals(Constants.SEARCH_NAVIGATION_ACTION)) {
                    indexView = DictionariesPool.getInstance().getIndexView(set, pageSize, viewOffset, searchValue, collator);
                    indexView.viewOffset = getOffset(indexView, searchValue, collator);
                } else if (action.equals(Constants.SEARCH_BY_INDEX_NAVIGATION_ACTION)) {
                    String shortDictionaryName = request.getParameter("sdn");
                    String searchField = request.getParameter("f");
                    searchValue = DictionariesPool.getInstance().findHeadwordByIndexedFieldValue(shortDictionaryName, searchField, searchValue);
                    indexView = DictionariesPool.getInstance().getIndexView(set, pageSize, viewOffset, searchValue, collator);
                    indexView.viewOffset = getOffset(indexView, searchValue, collator);
                }
            }

            Gson gson = new Gson();
            String s = gson.toJson(indexView);
            out.write(s);

        } catch (Exception e) {
            e.printStackTrace();
            out.write(e.getMessage());
        } finally {
            osw.flush();
            out.flush();
            osw.close();
            out.close();
        }
    }

    private int getOffset(IndexView indexView, String searchValue, Collator collator) {
        Iterator<String> it = indexView.index.iterator();
        int offset = 0;
        while (it.hasNext()) {
            String next = it.next();
            if (collator.compare(next,searchValue) >= 0) {
                return offset;
            }
            ++offset;
        }
        return offset;
    }

    private Collator getCollactor(Set<CompressedDictionary> set) {
        if (!set.isEmpty()) {
            Locale locale = new Locale(set.iterator().next().getProperties().getProperty(Constants.DICTIONARY_SOURCE_LANGUAGE));
            return Collator.getInstance(locale);
        } else {
            return null;
        }
    }

}
