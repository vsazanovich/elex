package com.google.code.elex.shared;

import com.google.code.elex.client.entry.*;

import java.io.*;

/**
 * Every time model is changed (non-incremental change, eg. delete or refactor), we need to change our data
 * <p/>
 * User: Vitaly Sazanovich
 * Date: 23/01/13
 * Time: 19:40
 * Email: Vitaly.Sazanovich@gmail.com
 */
public class ModelChangeJob {
    public static void main(String[] args) {
        try {
//            ControlServlet.CONTAINER = Container.Builder.buildContainerFromXmlOnInputStream(ModelChangeJob.class.getClassLoader().getResourceAsStream("context.xml"));
//            FileDAO fileDAO = ControlServlet.CONTAINER.getObjectThatImplementsOrNull(FileDAO.class);
//            String[] locales = fileDAO.getLocales();
//            for (String locale : locales) {
//                String pathName = fileDAO.getDataFolderName() + File.separator + locale + File.separator;
//                File f = new File(pathName);
//                processFile(f);
//            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public static void processFile(File f) throws Exception {
        if (f.isDirectory()) {
            File[] files = f.listFiles();
            for (File file : files) {
                processFile(file);
            }
        } else {
            if (f.getName().endsWith(".xml")) {
                BufferedReader in = new BufferedReader(new InputStreamReader(new FileInputStream(f), "UTF-8"));
                String s = "";
                StringBuffer sb = new StringBuffer();
                boolean firstLine = true;
                while ((s = in.readLine()) != null) {
                    if (!firstLine) {
                        sb.append("\n");
                    }
                    sb.append(s);
                    firstLine = false;
                }
                in.close();
                String content = sb.toString();
                System.out.println(f.getAbsoluteFile());
                EntryObjectModel entryObjectModel = (EntryObjectModel) ElexUtils.xml2entry(content);
                entryObjectModel = moveIdioms(entryObjectModel);
                String xml = ElexUtils.entry2xml(entryObjectModel);

                BufferedWriter out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(f), "UTF-8"));
                out.write(xml);
                out.close();
            }
        }
    }

    private static EntryObjectModel moveIdioms(EntryObjectModel entryObjectModel) {
        if (entryObjectModel.getIdioms() != null) {
            Idioom[] ids = entryObjectModel.getIdioms();
            for (Idioom id : ids) {
                IdiomExplanation ie = new IdiomExplanation();
                ie.setExplanation(id.getExplanation());
                ie.setMeta(id.getMeta());
                ie.setTranslations(id.getTranslations()==null||id.getTranslations().length==0?null:id.getTranslations());
                id.setIdiomExplanations(id.getExplanation()==null?null:new IdiomExplanation[]{ie});
                id.setExplanation(null);
                id.setTranslations(null);
                id.setMeta(null);

            }
        }
        if (entryObjectModel.getPartOfSpeeches() != null) {
            for (PartOfSpeech pos : entryObjectModel.getPartOfSpeeches()) {
                if (pos.getEntryDefinitions() != null) {
                    for (EntryDefinition ed : pos.getEntryDefinitions()) {
                        if (ed.getIdioms() != null) {
                            for (Idioom id : ed.getIdioms()) {
                                IdiomExplanation ie = new IdiomExplanation();
                                ie.setExplanation(id.getExplanation());
                                ie.setMeta(id.getMeta());
                                ie.setTranslations(id.getTranslations()==null||id.getTranslations().length==0?null:id.getTranslations());
                                id.setIdiomExplanations(id.getExplanation()==null?null:new IdiomExplanation[]{ie});
                                id.setExplanation(null);
                                id.setTranslations(null);
                                id.setMeta(null);
                            }
                        }
                    }
                }
            }
        }
        return entryObjectModel;
    }
}
