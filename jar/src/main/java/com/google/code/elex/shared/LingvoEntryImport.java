package com.google.code.elex.shared;

import com.google.code.elex.client.entry.EntryDefinition;
import com.google.code.elex.client.entry.EntryObjectModel;
import com.google.code.elex.client.entry.PartOfSpeech;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/**
 * User: Vitaly Sazanovich
 * Date: 02/01/13
 * Time: 16:19
 * Email: Vitaly.Sazanovich@gmail.com
 */
public class LingvoEntryImport {
    public static void main(String[] args) {
        String dic = "C:\\nl-nl_vandale_an_1_0d.dsl";
        try {
            FileInputStream fstream = new FileInputStream(dic);
            DataInputStream in = new DataInputStream(fstream);
            BufferedReader br = new BufferedReader(new InputStreamReader(in));
            String strLine;
            int counter = 0;

            skip(6, br);
            List<String> entries = new ArrayList<String>();
            StringBuffer entryBuffer = new StringBuffer();
            while ((strLine = br.readLine()) != null && counter++ < 100) {
                // Print the content on the console
                if (!strLine.startsWith("\t") && entryBuffer.length() > 0) {
                    dumpEntry(entryBuffer.toString());
                    entryBuffer = new StringBuffer();
                }
                entryBuffer.append(strLine);
                entryBuffer.append("\n");


            }
            in.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static int HEADWORD_LINE_TYPE = 0;
    public static int SYLLABLES_LINE_TYPE = 1;
    public static int DEFINITION_LINE_TYPE = 2;

    public static void dumpEntry(String entryText) {
        EntryObjectModel entry = new EntryObjectModel();
        String[] lines = entryText.split("\n");
        for (String line : lines) {
            if (!line.equals("")) {
                int type = type(line);
                List<EntryDefinition> eds = new ArrayList<EntryDefinition>();
                if (type == HEADWORD_LINE_TYPE) {
                    entry.setHeadword(line.trim());
                } else if (type == SYLLABLES_LINE_TYPE) {
                    entry.setSyllables(syllables(line));
                    entry.setStressedSyllable(stressed(line));
                } else if (type == DEFINITION_LINE_TYPE) {
                    eds.add(def(line));
                }
                if (eds.size() > 0) {
                    PartOfSpeech pos = new PartOfSpeech();
                    pos.setEntryDefinitions(eds.toArray(new EntryDefinition[eds.size()]));
                    entry.setPartOfSpeeches(new PartOfSpeech[]{pos});
                }
            }
        }
        System.out.println(entry);
    }

    private static EntryDefinition def(String line) {
        EntryDefinition ed = new EntryDefinition();
        String[] parts = line.split("(\\[c slateblue\\])|(\\[\\/c\\])");

        return ed;
    }

    private static int stressed(String line) {
        String[] parts = line.split("(\\{\\{ent\\}\\})|(\\{\\{\\/ent\\}\\})");
        String[] syls = parts[1].split("▪");
        for (int i = 0; i < syls.length; i++) {
            if (syls[i].contains("[u]")) {
                return i + 1;
            }
        }
        return 1;
    }

    public static String syllables(String line) {
        String[] parts = line.split("(\\{\\{ent\\}\\})|(\\{\\{\\/ent\\}\\})");
        return parts[1].replaceAll("\\[u\\]", "").replaceAll("\\[\\/u\\]", "").replaceAll("▪", "·");
    }


    public static int type(String entry) {
        if (!entry.startsWith("\t")) {
            return HEADWORD_LINE_TYPE;
        }
        if (entry.startsWith("\t[m1][b]•")) {
            return SYLLABLES_LINE_TYPE;
        }
        if (entry.startsWith("\t[m2][c firebrick]")) {
            return DEFINITION_LINE_TYPE;
        }
        return -1;
    }

    public static void skip(int count, BufferedReader br) throws Exception {
        for (int i = 0; i < count; i++) {
            br.readLine();
        }
    }
}
