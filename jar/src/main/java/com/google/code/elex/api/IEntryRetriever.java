package com.google.code.elex.api;

/**
 * Author: Vitaly Sazanovich
 * Email: Vitaly.Sazanovich@gmail.com
 * Date: 2/13/14
 */
public interface IEntryRetriever {
    public String retrieveHeadword(int offset) throws Exception;

    public String retrieveEntryByOffset(int offset, boolean includeHeadword) throws Exception;
}
