package com.google.code.elex.pack;

import java.io.*;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

/**
 * Author: Vitaly Sazanovich
 * Email: vitaly.sazanovich@gmail.com
 * Date: 05/12/13
 * <p/>
 * Reads DSL file, sorts entries according to the default Java comparator, writes result to a new DSL file.
 */
public class DslEntrySorter {

    public static void main(String[] args) {
        try {
            if (args.length != 2) {
                throw new Exception("2 args expected");
            }
            DslEntrySorter sorter = new DslEntrySorter();
            sorter.resort(args[0], args[1]);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void resort(String sourceFileLocation, String outputFile) throws Exception {
        SortedMap<String, String> entries = new TreeMap<String, String>();
        int counter = 1;


        BufferedReader in = new BufferedReader(new InputStreamReader(new FileInputStream(sourceFileLocation), "Unicode"));
        BufferedWriter out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(outputFile), "UTF-8"));

        String str = in.readLine();
        //skipping header
        while (str != null && str.startsWith("#")) {
            out.write(str + '\n');
            str = in.readLine();
        }

        while (str != null && !str.equals("")) {
            //expecting header
            String hw = str;
            StringBuilder content = new StringBuilder();
            str = in.readLine();
            while (str!=null && str.startsWith("\t")) {
                content.append(str + '\n');
                str = in.readLine();
            }
            entries.put(hw, content.toString());
            if (counter % 1000 == 0) {
                System.out.println(counter);
            }
            ++counter;
        }


        for (Map.Entry<String, String> e : entries.entrySet()) {
            String key = e.getKey() + '\n';
            String value = e.getValue() ;
            out.write(key);
            out.write(value);
        }

        in.close();
        out.close();
    }
}
