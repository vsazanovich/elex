package com.google.code.elex.model;

import java.io.Serializable;
import java.util.SortedSet;
import java.util.TreeSet;

/**
 * Author: Vitaly Sazanovich
 * Email: vitaly.sazanovich@gmail.com
 * Date: 13/02/14
 * Time: 16:06
 */
public class IndexView implements Serializable {
    public SortedSet<String> index;
    public boolean isStartReached = false;
    public boolean isEndReached = false;
    public int viewOffset;
}
