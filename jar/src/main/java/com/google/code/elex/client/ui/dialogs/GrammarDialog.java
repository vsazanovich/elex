package com.google.code.elex.client.ui.dialogs;

import com.google.code.elex.client.MyThreadLocal;
import com.google.code.elex.client.entry.Grammar;
import com.google.code.elex.client.ui.AbstractContainerRenderer;
import com.google.code.elex.client.ui.Bindable;
import com.google.code.elex.client.ui.KeyValuePair;
import com.google.code.elex.client.ui.comboboxmodels.HelpWerkwoordComboBoxModel;
import com.google.code.elex.client.ui.comboboxmodels.VoornaamwoordSoortComboBoxModel;
import com.google.code.elex.client.ui.comboboxmodels.WerkwoordSoortComboBoxModel;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.util.Arrays;
import java.util.List;


/**
 * User: Vitaly Sazanovich
 * Date: 12/18/12
 * Time: 6:39 PM
 */
public class GrammarDialog extends AbstractContainerRenderer {
    public JPanel cardPanel;
    public Bindable parent;
    private String cardName;

    public JCheckBox mCheckBox;
    public JCheckBox mvCheckBox;
    public JCheckBox vCheckBox;
    public JCheckBox oCheckBox;
    public JComboBox verbType;
    public JComboBox helpVerb;
    public JTextField plural;
    public JTextField verleden;
    public JTextField voltooid;
    public JTextField comparative;
    public JTextField superlative;
    public JCheckBox alleenAttributief;
    public JCheckBox alleenPredikatief;
    public JCheckBox ookAbsoluut;
    public JCheckBox alleenEnkel;
    public JCheckBox alleenMeer;
    public JComboBox vnwKey;

    public GrammarDialog(Bindable p, Grammar grammar, String cn) {
        parent = p;
        cardName = cn;
        String grammarDialogDescriptor = "com/dictiography/client/ui/descriptors/" + MyThreadLocal.get().getLang() + "/GrammarDialog.xml";
        if (getClass().getClassLoader().getResourceAsStream(grammarDialogDescriptor) == null) {
            return;
        }
        init(grammarDialogDescriptor);

        CardLayout cl = (CardLayout) (cardPanel.getLayout());
        cl.show(cardPanel, cardName);
        if (grammar != null) {
            setData(grammar);
        }
        ((JDialog) container).pack();
        container.setVisible(true);
    }

    public Action saveAction = new AbstractAction() {
        public void actionPerformed(ActionEvent e) {
            parent.setData(getData(null));
            ((JDialog) container).dispose();
        }
    };


    public static void main(String[] args) {
        new GrammarDialog(null, null, "znw");
    }


    @Override
    public void setData(Object d) {
        if (d == null) return;
        Grammar data = (Grammar) d;
        if (data.getGenderKey() != null) {
            List<String> s = Arrays.asList(data.getGenderKey().split(","));
            if (mCheckBox != null) mCheckBox.setSelected(data.getGenderKey().equals("m") || s.contains("m"));
            if (mvCheckBox != null) mvCheckBox.setSelected(data.getGenderKey().equals("mv") || s.contains("mv"));
            if (vCheckBox != null) vCheckBox.setSelected(data.getGenderKey().equals("v") || s.contains("v"));
            if (oCheckBox != null) oCheckBox.setSelected(data.getGenderKey().equals("o") || s.contains("o"));
        }
        if (data.getPlural() != null && plural != null) plural.setText(data.getPlural());
        if (data.getVerbType() != null && verbType != null)
            verbType.setSelectedItem(((WerkwoordSoortComboBoxModel) verbType.getModel()).get(data.getVerbType()));
        if (data.getVerleden() != null && verleden != null) verleden.setText(data.getVerleden());
        if (data.getHelpVerb() != null && helpVerb != null)
            helpVerb.setSelectedItem(((HelpWerkwoordComboBoxModel) helpVerb.getModel()).get(data.getHelpVerb()));
        if (data.getVoltooid() != null && voltooid != null) voltooid.setText(data.getVoltooid());
        if (data.getComparative() != null && comparative != null) comparative.setText(data.getComparative());
        if (data.getSuperlative() != null && superlative != null) superlative.setText(data.getSuperlative());
        if (data.getAlleenAttributief() != null && alleenAttributief != null)
            alleenAttributief.setSelected(data.getAlleenAttributief().booleanValue());
        if (data.getAlleenPredikatief() != null && alleenPredikatief != null)
            alleenPredikatief.setSelected(data.getAlleenPredikatief().booleanValue());
        if (data.getOokAbsoluut() != null && ookAbsoluut != null)
            ookAbsoluut.setSelected(data.getOokAbsoluut().booleanValue());
        if (data.getAlleenEnkel() != null && alleenEnkel != null)
            alleenEnkel.setSelected(data.getAlleenEnkel().booleanValue());
        if (data.getAlleenMeer() != null && alleenMeer != null)
            alleenMeer.setSelected(data.getAlleenMeer().booleanValue());
        if (data.getVnwKey() != null && vnwKey != null)
            vnwKey.setSelectedItem(((VoornaamwoordSoortComboBoxModel) vnwKey.getModel()).get(data.getVnwKey()));
    }

    @Override
    public Object getData(Object d) {
        Grammar data = (Grammar) d;
        if (data == null) data = new Grammar();

        StringBuffer sb = new StringBuffer();
        if (mCheckBox != null && mCheckBox.isSelected()) sb.append("m,");
        if (mvCheckBox != null && mvCheckBox.isSelected()) sb.append("mv,");
        if (vCheckBox != null && vCheckBox.isSelected()) sb.append("v,");
        if (oCheckBox != null && oCheckBox.isSelected()) sb.append("o");
        String res = sb.toString();
        if (res.endsWith(",")) res = res.substring(0, res.length() - 1);
        if (res.length() > 0) data.setGenderKey(res);

        if (plural != null && !plural.getText().trim().equals("")) data.setPlural(plural.getText().trim());
        if (alleenEnkel != null && alleenEnkel.isSelected()) data.setAlleenEnkel(true);
        if (alleenMeer != null && alleenMeer.isSelected()) data.setAlleenMeer(true);

        if (verbType != null) data.setVerbType(((KeyValuePair) verbType.getSelectedItem()).getKey());
        if (verleden != null && !verleden.getText().trim().equals("")) data.setVerleden(verleden.getText().trim());
        if (helpVerb != null) data.setHelpVerb(((KeyValuePair) helpVerb.getSelectedItem()).getKey());
        if (voltooid != null && !voltooid.getText().trim().equals("")) data.setVoltooid(voltooid.getText().trim());
        if (ookAbsoluut != null && ookAbsoluut.isSelected()) data.setOokAbsoluut(true);

        if (comparative != null && !comparative.getText().trim().equals(""))
            data.setComparative(comparative.getText().trim());
        if (superlative != null && !superlative.getText().trim().equals(""))
            data.setSuperlative(superlative.getText().trim());
        if (alleenAttributief != null && alleenAttributief.isSelected()) data.setAlleenAttributief(true);
        if (alleenPredikatief != null && alleenPredikatief.isSelected()) data.setAlleenPredikatief(true);

        if (vnwKey != null) data.setVnwKey(((KeyValuePair) vnwKey.getSelectedItem()).getKey());

        return data;
    }

    @Override
    public boolean isEmpty() {
        if (mCheckBox != null && mCheckBox.isSelected()) return false;
        if (mvCheckBox != null && mvCheckBox.isSelected()) return false;
        if (vCheckBox != null && vCheckBox.isSelected()) return false;
        if (oCheckBox != null && oCheckBox.isSelected()) return false;
        if (alleenEnkel != null && alleenEnkel.isSelected()) return false;
        if (alleenMeer != null && alleenMeer.isSelected()) return false;
        if (ookAbsoluut != null && ookAbsoluut.isSelected()) return false;
        if (alleenAttributief != null && alleenAttributief.isSelected()) return false;
        if (alleenPredikatief != null && alleenPredikatief.isSelected()) return false;

        if (plural != null && !plural.getText().trim().equals("")) return false;
        if (verleden != null && !verleden.getText().trim().equals("")) return false;
        if (voltooid != null && !voltooid.getText().trim().equals("")) return false;
        if (comparative != null && !comparative.getText().trim().equals("")) return false;
        if (superlative != null && !superlative.getText().trim().equals("")) return false;

        return true;
    }

}
