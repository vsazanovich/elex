package com.google.code.elex;

import org.apache.commons.io.IOUtils;
import winstone.Launcher;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.*;

/**
 * Author: Vitaly Sazanovich
 * Email: Vitaly.Sazanovich@gmail.com
 * Date: 2/16/14
 */
public class ElexMain {
    public static final String ELEX_SELECTED_KEY = "actionConstants.selected";
    private final static int TIMEOUT = 10000;
    public static ElexMain INSTANCE;
    MessageConsole mc;

    public ElexMain(final String[] args) {


        //Create and set up the window.
        JFrame frame = new JFrame("Elex Console");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);


        Action makerAction = new AbstractAction("maker",
                new ImageIcon(getImageBytes("application_form_edit.png"))) {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.out.println("maker");
            }
        };
        Action packagerAction = new AbstractAction("packager",
                new ImageIcon(getImageBytes("compress.png"))) {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.out.println("packager");
            }
        };


        Action goUrlAction = new AbstractAction("goHome", new ImageIcon(getImageBytes("book_open.png"))) {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    openWebpage(new URL("http://localhost:8080/index.html"));
                } catch (MalformedURLException e1) {
                    e1.printStackTrace();
                }
            }
        };

        Action goHomeAction = new AbstractAction("goHome", new ImageIcon(getImageBytes("help.png"))) {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    openWebpage(new URL("https://bitbucket.org/vsazanovich/elex/wiki/Home"));
                } catch (MalformedURLException e1) {
                    e1.printStackTrace();
                }
            }
        };

        Action exitAction = new AbstractAction("exitAction", new ImageIcon(getImageBytes("door_in.png"))) {
            @Override
            public void actionPerformed(ActionEvent e) {
                shutdown();
            }
        };

        frame.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent winEvt) {
                shutdown();
            }
        });


        JToolBar toolBar = new JToolBar("Formatting");
        toolBar.add(goUrlAction);
        toolBar.add(makerAction);
        toolBar.add(packagerAction);
        toolBar.add(new JSeparator(JSeparator.VERTICAL));
        toolBar.add(goHomeAction);
        toolBar.add(exitAction);
        toolBar.setFloatable(false);


        JTextArea textComponent = new JTextArea();

        mc = new MessageConsole(textComponent);
        mc.redirectOut();
        mc.redirectErr(Color.RED, null);
        mc.setMessageLines(1000);


        frame.getContentPane().add(toolBar, BorderLayout.NORTH);
        frame.getContentPane().add(new JScrollPane(textComponent), BorderLayout.CENTER);

        frame.setSize(800, 600);
        //Display the window.
//        frame.pack();
        frame.setVisible(true);


        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                try {
                    Launcher.main(args);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public static void shutdown() {
        Socket socket = null;
        try {
            socket = new Socket("localhost", Integer.parseInt("8081"));
            socket.setSoTimeout(TIMEOUT);
            OutputStream out = socket.getOutputStream();
            out.write(Launcher.SHUTDOWN_TYPE);
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            if (socket != null) {
                try {
                    socket.close();
                } catch (IOException ioex) {
                    ioex.printStackTrace();
                }
            }
        }


        try {
            while (true) {
                boolean avail = available(8080);
                if (avail) {
                    System.exit(0);
                } else {
                    System.out.println("Waiting for the server to stop...");
                    Thread.sleep(1000);
                }
            }
        } catch (InterruptedException e1) {
            e1.printStackTrace();
        }

    }

    /**
     * Checks to see if a specific port is available.
     *
     * @param port the port to check for availability
     */
    public static boolean available(int port) {
        ServerSocket ss = null;
        DatagramSocket ds = null;
        try {
            ss = new ServerSocket(port);
            ss.setReuseAddress(true);
            ds = new DatagramSocket(port);
            ds.setReuseAddress(true);
            return true;
        } catch (IOException e) {
        } finally {
            if (ds != null) {
                ds.close();
            }

            if (ss != null) {
                try {
                    ss.close();
                } catch (IOException e) {
                    /* should not be thrown */
                }
            }
        }

        return false;
    }


    public static void openWebpage(URI uri) {
        Desktop desktop = Desktop.isDesktopSupported() ? Desktop.getDesktop() : null;
        if (desktop != null && desktop.isSupported(Desktop.Action.BROWSE)) {
            try {
                desktop.browse(uri);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public static void openWebpage(URL url) {
        try {
            openWebpage(url.toURI());
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
    }


    public byte[] getImageBytes(String fileName) {
        try {
            InputStream is = ElexMain.class.getClassLoader().getResourceAsStream(fileName);
            byte[] bytes = IOUtils.toByteArray(is);
            return bytes;
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return new byte[]{};
    }

    /**
     * Create the GUI and show it.  For thread safety,
     * this method should be invoked from the
     * event-dispatching thread.
     */
    private static void createAndShowGUI(final String[] args) {
        INSTANCE = new ElexMain(args);
    }


    public static void main(final String[] args) {
        //Schedule a job for the event-dispatching thread:
        //creating and showing this application's GUI.
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                createAndShowGUI(args);
            }
        });
    }

}
