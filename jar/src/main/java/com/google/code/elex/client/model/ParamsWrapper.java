package com.google.code.elex.client.model;

import com.google.code.elex.shared.ElexConstants;

/**
 * User: Vitaly Sazanovich
 * Date: 30/10/12
 * Time: 11:51
 * Email: Vitaly.Sazanovich@gmail.com
 */
public class ParamsWrapper {
    public String clientVersion;
    public String modelVersion;
    public Settings settings;
    public Integer selectedEntryId;
    public Integer selectedLocalPositionInIndex;
    public Integer selectedGlobalPositionInIndex;
    public String searchValue;
    public String locale;
    public IndexModel indexModel;
    public DictionaryEntry dictionaryEntry;
    public String initiator;
    public String action = ElexConstants.NEW_ACTION;
}
