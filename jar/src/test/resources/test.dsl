#NAME	"Webster"
#INDEX_LANGUAGE	"English"
#CONTENTS_LANGUAGE	"English"
-able
	[m1][i][c][trn]·-[/c][/i] An adjective suffix now usually in a passive sense; able to be; fit to be; expressing capacity or worthiness in a passive sense; as, movable, able to be moved; amendable, able to be amended; blamable, fit to be blamed; salable.[/trn][/m]
-ably
	[m1][i][c][trn]·-[/c][/i] A suffix composed of -able and the adverbial suffix -ly; as, favorably.[/trn][/m]
-ana
	[m1][i][c][trn]·-[/c][/i] A suffix to names of persons or places, used to denote a collection of notable sayings, literary gossip, anecdotes, [i][c]·etc.[/c][/i] Thus, Scaligerana is a book containing the sayings of Scaliger, Johnsoniana of Johnson, [i][c]·etc.[/trn][/c][/i][/m]
-ance
	[m1][i][c][trn]·-[/c][/i] A suffix signifying action; also, quality or state; as, assistance, resistance, appearance, elegance. [i][c]·see[/c][/i] [u]-ancy[/u].[/trn][/m]
-ancy
	[m1][i][c][trn]·-[/c][/i] A suffix expressing more strongly than -ance the idea of quality or state; as, constancy, buoyancy, infancy.[/trn][/m]
-ant
	[m1][i][c][trn]·-[/c][/i] A suffix sometimes marking the agent for action; as, merchant, covenant, servant, pleasant, [i][c]·etc.[/c][/i] [i][c]·cf.[/c][/i] [u]-ent[/u].[/trn][/m]