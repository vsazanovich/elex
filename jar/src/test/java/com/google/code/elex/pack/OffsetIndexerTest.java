package com.google.code.elex.pack;

import junit.framework.TestCase;
import org.apache.commons.io.IOUtils;

import java.io.*;

/**
 * Author: Vitaly Sazanovich
 * Email: vitaly.sazanovich@gmail.com
 * Date: 27/11/13
 */
public class OffsetIndexerTest extends TestCase {
    public void testIndexDsl() {
        try {
            File in = File.createTempFile("testinputfile", ".tmp");
            InputStream inputStream = getClass().getClassLoader().getResourceAsStream("test.dsl");
            byte[] bbs = IOUtils.toByteArray(inputStream);
            in.deleteOnExit();
            FileOutputStream fos = new FileOutputStream(in);
            fos.write(bbs);
            fos.close();

            File out = File.createTempFile("testoutputfile", ".tmp");

            OffsetIndexer offsetIndexer = new OffsetIndexer();
            offsetIndexer.indexDsl(in.getAbsolutePath(), out.getAbsolutePath());

            FileInputStream fis = new FileInputStream(out);
            assertEquals(7 * 4, fis.available());

        } catch (IOException e) {
            e.printStackTrace();
            fail();
        }


    }
}
