package com.google.code.elex.model;

import com.google.code.elex.BaseTestCase;

import java.text.Collator;
import java.util.Locale;

/**
 * Author: Vitaly Sazanovich
 * Email: vitaly.sazanovich@gmail.com
 * Date: 27/11/13
 */
public class CompressedDictionaryTest extends BaseTestCase {

    public void testFindInsertionPoint() throws Exception {
        CompressedDictionary cd = new CompressedDictionary(Collator.getInstance(Locale.ENGLISH));

        String[] hws = new String[]{"b"};
        cd.size = hws.length;
        cd.entryRetriever = new TestEntryRetriever(hws);

        assertEquals(0, cd.findInsertionPoint("b"));
        assertEquals(-1, cd.findInsertionPoint("a"));
        assertEquals(-2, cd.findInsertionPoint("c"));
        assertEquals(-2, cd.findInsertionPoint("d"));

        hws = new String[]{};
        cd.size = hws.length;
        cd.entryRetriever = new TestEntryRetriever(hws);
        assertEquals(-1, cd.findInsertionPoint("b"));
        assertEquals(-1, cd.findInsertionPoint("a"));

        hws = new String[]{"b", "c"};
        cd.size = hws.length;
        cd.entryRetriever = new TestEntryRetriever(hws);
        assertEquals(0, cd.findInsertionPoint("b"));
        assertEquals(1, cd.findInsertionPoint("c"));
        assertEquals(-1, cd.findInsertionPoint("a"));
        assertEquals(-3, cd.findInsertionPoint("d"));
    }


}
