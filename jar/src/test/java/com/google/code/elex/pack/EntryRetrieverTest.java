package com.google.code.elex.pack;

import junit.framework.TestCase;
import org.apache.commons.io.IOUtils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.RandomAccessFile;

/**
 * Author: Vitaly Sazanovich
 * Email: vitaly.sazanovich@gmail.com
 * Date: 27/11/13
 */
public class EntryRetrieverTest extends TestCase {
    public void testEntryRetrieve() {
        try {
            File in = File.createTempFile("testinputfile", ".tmp");
            InputStream inputStream = getClass().getClassLoader().getResourceAsStream("test.dsl");
            byte[] bbs = IOUtils.toByteArray(inputStream);
            in.deleteOnExit();
            FileOutputStream fos = new FileOutputStream(in);
            fos.write(bbs);
            fos.close();

            File out = File.createTempFile("testoutputfile", ".tmp");

            OffsetIndexer offsetIndexer = new OffsetIndexer();
            offsetIndexer.indexDsl(in.getAbsolutePath(), out.getAbsolutePath());

            EntryRetriever entryRetriever = new EntryRetriever();
            String entry = entryRetriever.retrieveEntry(new RandomAccessFile(in.getAbsolutePath(), "r"), new RandomAccessFile(out.getAbsolutePath(), "r"), 6);
            System.out.println(entry);

            entry = entryRetriever.retrieveEntry(new RandomAccessFile(in.getAbsolutePath(), "r"), new RandomAccessFile(out.getAbsolutePath(), "r"), 7);
            assertEquals(entry,null);

        } catch (Exception e) {
            e.printStackTrace();
            fail();
        }
    }
}
