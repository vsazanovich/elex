package com.google.code.elex.model;

import com.google.code.elex.BaseTestCase;
import com.google.code.elex.api.IPeekIterator;

import java.text.Collator;
import java.util.HashSet;
import java.util.Locale;
import java.util.Set;

/**
 * Author: Vitaly Sazanovich
 * Email: Vitaly.Sazanovich@gmail.com
 * Date: 2/13/14
 */
public class IteratorsWrapperTest extends BaseTestCase {
    Collator collator = Collator.getInstance(Locale.ENGLISH);

    public void testIteratorOneDictionary() throws Exception {
        CompressedDictionary cd = new CompressedDictionary(Collator.getInstance(Locale.ENGLISH));
        String[] hws;
        IPeekIterator ii;
        //empty dictionary
        hws = new String[]{};
        cd.size = hws.length;
        cd.entryRetriever = new TestEntryRetriever(hws);

        Set<CompressedDictionary> set = new HashSet<CompressedDictionary>();

        set.add(cd);

        ii = new IteratorsWrapper(set, "b", true, collator);
        assertEquals(ii.hasNext(), false);
        assertEquals(ii.peek(), null);
        assertEquals(ii.next(), null);

        hws = new String[]{"b", "c", "d"};
        cd.size = hws.length;
        cd.entryRetriever = new TestEntryRetriever(hws);
        set = new HashSet<CompressedDictionary>();
        set.add(cd);
        //exact match forward from start
        ii = new IteratorsWrapper(set, "b", true, collator);
        assertEquals(true, ii.hasNext());
        assertEquals("b", ii.peek());
        assertEquals("b", ii.next());
        assertEquals("c", ii.next());
        assertEquals("d", ii.peek());
        assertEquals("d", ii.next());
        assertEquals(null, ii.next());
        //exact match backward from end
        ii = new IteratorsWrapper(set, "d", false, collator);
        assertEquals(true, ii.hasNext());
        assertEquals("d", ii.peek());
        assertEquals("d", ii.next());
        assertEquals("c", ii.next());
        assertEquals("b", ii.peek());
        assertEquals("b", ii.next());
        assertEquals(null, ii.next());

        hws = new String[]{"b", "c", "d"};
        cd.size = hws.length;
        cd.entryRetriever = new TestEntryRetriever(hws);
        set = new HashSet<CompressedDictionary>();
        set.add(cd);
        //no match forward from start
        ii = new IteratorsWrapper(set, "a", true, collator);
        assertEquals(true, ii.hasNext());
        assertEquals("b", ii.peek());
        assertEquals("b", ii.next());
        assertEquals("c", ii.next());
        assertEquals("d", ii.peek());
        assertEquals("d", ii.next());
        assertEquals(null, ii.next());
        //no match forward from center
        ii = new IteratorsWrapper(set, "bb", true, collator);
        assertEquals(true, ii.hasNext());
        assertEquals("c", ii.peek());
        assertEquals("c", ii.next());
        assertEquals("d", ii.next());
        assertEquals(null, ii.next());
        //no match forward from end
        ii = new IteratorsWrapper(set, "dd", true, collator);
        assertEquals(false, ii.hasNext());
        assertEquals(null, ii.next());

        //no match backward from end
        ii = new IteratorsWrapper(set, "dd", false, collator);
        assertEquals(true, ii.hasNext());
        assertEquals("d", ii.peek());
        assertEquals("d", ii.next());
        assertEquals("c", ii.next());
        assertEquals("b", ii.peek());
        assertEquals("b", ii.next());
        assertEquals(null, ii.next());
        //no match backward from center
        ii = new IteratorsWrapper(set, "cc", false, collator);
        assertEquals(true, ii.hasNext());
        assertEquals("c", ii.peek());
        assertEquals("c", ii.next());
        assertEquals("b", ii.next());
        assertEquals(null, ii.next());
        //no match backward from start
        ii = new IteratorsWrapper(set, "a", false, collator);
        assertEquals(false, ii.hasNext());
        assertEquals(null, ii.next());
    }

    public void testIteratorTwoDictionaries() throws Exception {
        CompressedDictionary cd1 = new CompressedDictionary(Collator.getInstance(Locale.ENGLISH));
        CompressedDictionary cd2 = new CompressedDictionary(Collator.getInstance(Locale.ENGLISH));
        String[] hws1, hws2;
        IPeekIterator ii;
        //empty dictionaries
        hws1 = new String[]{};
        cd1.size = hws1.length;
        cd1.entryRetriever = new TestEntryRetriever(hws1);
        hws2 = new String[]{};
        cd2.size = hws2.length;
        cd2.entryRetriever = new TestEntryRetriever(hws2);

        Set<CompressedDictionary> set = new HashSet<CompressedDictionary>();
        set.add(cd1);
        set.add(cd2);

        ii = new IteratorsWrapper(set, "b", true, collator);
        assertEquals(ii.hasNext(), false);
        assertEquals(ii.peek(), null);
        assertEquals(ii.next(), null);

        //mix
        hws1 = new String[]{"b", "d"};
        cd1.size = hws1.length;
        cd1.entryRetriever = new TestEntryRetriever(hws1);
        hws2 = new String[]{"a", "c", "d"};
        cd2.size = hws2.length;
        cd2.entryRetriever = new TestEntryRetriever(hws2);
        set = new HashSet<CompressedDictionary>();
        set.add(cd1);
        set.add(cd2);

        //EXACT MATCH

        //exact match forward from start
        ii = new IteratorsWrapper(set, "a", true, collator);
        assertEquals(true, ii.hasNext());
        assertEquals("a", ii.peek());
        assertEquals("a", ii.next());
        assertEquals("b", ii.next());
        assertEquals("c", ii.peek());
        assertEquals("c", ii.next());
        assertEquals("d", ii.next());
        assertEquals(null, ii.next());

        //exact match forward from center
        ii = new IteratorsWrapper(set, "c", true, collator);
        assertEquals(true, ii.hasNext());
        assertEquals("c", ii.peek());
        assertEquals("c", ii.next());
        assertEquals("d", ii.next());
        assertEquals(null, ii.next());

        //exact match forward from end
        ii = new IteratorsWrapper(set, "d", true, collator);
        assertEquals(true, ii.hasNext());
        assertEquals("d", ii.peek());
        assertEquals("d", ii.next());
        assertEquals(null, ii.next());

        //exact match backward from end
        ii = new IteratorsWrapper(set, "d", false, collator);
        assertEquals(true, ii.hasNext());
        assertEquals("d", ii.peek());
        assertEquals("d", ii.next());
        assertEquals("c", ii.next());
        assertEquals("b", ii.peek());
        assertEquals("b", ii.next());
        assertEquals("a", ii.next());
        assertEquals(null, ii.next());

        //exact match backward from center
        ii = new IteratorsWrapper(set, "c", false, collator);
        assertEquals(true, ii.hasNext());
        assertEquals("c", ii.peek());
        assertEquals("c", ii.next());
        assertEquals("b", ii.next());
        assertEquals("a", ii.next());
        assertEquals(null, ii.next());

        //exact match backward from end
        ii = new IteratorsWrapper(set, "a", false, collator);
        assertEquals(true, ii.hasNext());
        assertEquals("a", ii.peek());
        assertEquals("a", ii.next());
        assertEquals(null, ii.next());

        //NO MATCH

        //no match forward from start
        ii = new IteratorsWrapper(set, "0", true, collator);
        assertEquals(true, ii.hasNext());
        assertEquals("a", ii.peek());
        assertEquals("a", ii.next());
        assertEquals("b", ii.next());
        assertEquals("c", ii.peek());
        assertEquals("c", ii.next());
        assertEquals("d", ii.next());
        assertEquals(null, ii.next());

        //no match forward from center
        ii = new IteratorsWrapper(set, "bb", true, collator);
        assertEquals(true, ii.hasNext());
        assertEquals("c", ii.peek());
        assertEquals("c", ii.next());
        assertEquals("d", ii.next());
        assertEquals(null, ii.next());

        //no match forward from end
        ii = new IteratorsWrapper(set, "dd", true, collator);
        assertEquals(false, ii.hasNext());
        assertEquals(null, ii.peek());
        assertEquals(null, ii.next());

        //no match backward from end
        ii = new IteratorsWrapper(set, "dd", false, collator);
        assertEquals(true, ii.hasNext());
        assertEquals("d", ii.peek());
        assertEquals("d", ii.next());
        assertEquals("c", ii.next());
        assertEquals("b", ii.peek());
        assertEquals("b", ii.next());
        assertEquals("a", ii.next());
        assertEquals(null, ii.next());

        //no match backward from center
        ii = new IteratorsWrapper(set, "bb", false, collator);
        assertEquals(true, ii.hasNext());
        assertEquals("b", ii.peek());
        assertEquals("b", ii.next());
        assertEquals("a", ii.next());
        assertEquals(null, ii.next());

        //no match backward from start
        ii = new IteratorsWrapper(set, "0", false, collator);
        assertEquals(false, ii.hasNext());
        assertEquals(null, ii.peek());
        assertEquals(null, ii.next());

    }
}
