package com.google.code.elex.model;

import com.google.code.elex.BaseTestCase;
import com.google.code.elex.api.IPeekIterator;

import java.text.Collator;
import java.util.Locale;

/**
 * Author: Vitaly Sazanovich
 * Email: Vitaly.Sazanovich@gmail.com
 * Date: 2/13/14
 */
public class IndexIteratorTest extends BaseTestCase {

    public void testIterator() throws Exception {
        CompressedDictionary cd = new CompressedDictionary(Collator.getInstance(Locale.ENGLISH));
        String[] hws;
        IPeekIterator ii;
        //empty dictionary
        hws = new String[]{};
        cd.size = hws.length;
        cd.entryRetriever = new TestEntryRetriever(hws);

        ii = new IndexIterator(cd, "b", true);
        assertEquals(ii.hasNext(), false);
        assertEquals(ii.peek(), null);
        assertEquals(ii.next(), null);

        hws = new String[]{"b", "c", "d"};
        cd.size = hws.length;
        cd.entryRetriever = new TestEntryRetriever(hws);
        //exact match forward from start
        ii = new IndexIterator(cd, "b", true);
        assertEquals(true, ii.hasNext());
        assertEquals("b", ii.peek());
        assertEquals("b", ii.next());
        assertEquals("c", ii.next());
        assertEquals("d", ii.peek());
        assertEquals("d", ii.next());
        assertEquals(null, ii.next());
        //exact match backward from end
        ii = new IndexIterator(cd, "d", false);
        assertEquals(true, ii.hasNext());
        assertEquals("d", ii.peek());
        assertEquals("d", ii.next());
        assertEquals("c", ii.next());
        assertEquals("b", ii.peek());
        assertEquals("b", ii.next());
        assertEquals(null, ii.next());

        hws = new String[]{"b", "c", "d"};
        cd.size = hws.length;
        cd.entryRetriever = new TestEntryRetriever(hws);
        //no match forward from start
        ii = new IndexIterator(cd, "a", true);
        assertEquals(true, ii.hasNext());
        assertEquals("b", ii.peek());
        assertEquals("b", ii.next());
        assertEquals("c", ii.next());
        assertEquals("d", ii.peek());
        assertEquals("d", ii.next());
        assertEquals(null, ii.next());
        //no match forward from center
        ii = new IndexIterator(cd, "bb", true);
        assertEquals(true, ii.hasNext());
        assertEquals("c", ii.peek());
        assertEquals("c", ii.next());
        assertEquals("d", ii.next());
        assertEquals(null, ii.next());
        //no match forward from end
        ii = new IndexIterator(cd, "dd", true);
        assertEquals(false, ii.hasNext());
        assertEquals(null, ii.next());

        //no match backward from end
        ii = new IndexIterator(cd, "dd", false);
        assertEquals(true, ii.hasNext());
        assertEquals("d", ii.peek());
        assertEquals("d", ii.next());
        assertEquals("c", ii.next());
        assertEquals("b", ii.peek());
        assertEquals("b", ii.next());
        assertEquals(null, ii.next());
        //no match backward from center
        ii = new IndexIterator(cd, "cc", false);
        assertEquals(true, ii.hasNext());
        assertEquals("c", ii.peek());
        assertEquals("c", ii.next());
        assertEquals("b", ii.next());
        assertEquals(null, ii.next());
        //no match backward from start
        ii = new IndexIterator(cd, "a", false);
        assertEquals(false, ii.hasNext());
        assertEquals(null, ii.next());


    }
}
