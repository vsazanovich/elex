package com.google.code.elex.model;

import com.google.code.elex.BaseTestCase;
import com.google.code.elex.service.ElexUtils;
import org.apache.commons.io.IOUtils;

import java.io.*;
import java.util.List;

/**
 * Author: Vitaly Sazanovich
 * Email: vitaly.sazanovich@gmail.com
 * Date: 18/02/14
 */
public class ResourcesPackageTest extends BaseTestCase {
    static String[] EXPECTED = new String[]{
            "1/3d_glasses.png->9e4419c6cde5353ad6e2625db0c8b41c",
            "1/64_bit.png->51f0dd9a31fdaeed7c87e88b13d6dc57",
            "1/accept.png->63081da263743a49286950e07f957af0",
            "2/accordion.png->bebb22e6f93df56dd3f134bda9fddd7f",
            "2/account_balances.png->90c79fe78662fbc42fa8acd7b9fb9a12",
            "2/account_functions.png->c92c144be6c471fea346b1aeabd358b6",
            "3/acorn.png->4ddbaacad0492622002dc79f8b263378",
            "3/acoustic_guitar.png->5abffda2e23bb2d9b97929dc2d235137",
            "3/action_log.png->0431ef253180456cc966319e820dd158",
            "32_bit.png->9e951f2947681722e2e378b55898301c"};

    public void testSampleRes() throws Exception {
        File tmp = File.createTempFile("test", null);
        FileOutputStream fos = new FileOutputStream(tmp);
        tmp.deleteOnExit();
        InputStream is = getClass().getClassLoader().getResourceAsStream("test.res");
        byte[] bbs = new byte[is.available()];
        IOUtils.readFully(is, bbs);
        IOUtils.write(bbs, fos);

        ResourcesPackage rp = new ResourcesPackage(tmp.getAbsolutePath());
        System.out.println(rp.debug());
        RandomAccessFile raf = new RandomAccessFile(rp.getAbsolutePath(), "r");
        List<String> fns = rp.getFileNames();
        int counter = 0;
        for (String fn : fns) {
            int res = rp.findCountByName(raf, fn);
            assertEquals(counter, res);
            ++counter;
        }
        for (String s : EXPECTED) {
            String fn = s.split("->")[0];
            String sum1 = s.split("->")[1];
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            rp.getResourceByName(baos,fn);
            ByteArrayInputStream bais = new ByteArrayInputStream(baos.toByteArray());
            String sum2 = ElexUtils.getMD5Checksum(bais);
            assertEquals(sum1, sum2);
        }

    }
}
