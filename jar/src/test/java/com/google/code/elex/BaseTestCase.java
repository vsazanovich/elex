package com.google.code.elex;

import com.google.code.elex.api.IEntryRetriever;
import junit.framework.TestCase;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

/**
 * Author: Vitaly Sazanovich
 * Email: Vitaly.Sazanovich@gmail.com
 * Date: 2/13/14
 */
public abstract class BaseTestCase extends TestCase {
    public class TestEntryRetriever implements IEntryRetriever {
        String[] headwords;

        public TestEntryRetriever(String[] hws) {
            this.headwords = hws;
        }

        @Override
        public String retrieveHeadword(int offset) throws Exception {
            return headwords[offset];
        }

        @Override
        public String retrieveEntryByOffset(int offset, boolean b) throws Exception {
            throw new NotImplementedException();
        }
    }
}
